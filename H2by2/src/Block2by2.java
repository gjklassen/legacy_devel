import java.applet.*;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import java.awt.geom.Point2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.Random;
import java.awt.Color;
import java.math.*;
import java.net.*;
import java.awt.Color;
import javax.swing.JOptionPane;
import java.awt.Image.*;
import java.awt.image.BufferedImage;
import java.util.StringTokenizer;

public class Block2by2 extends Applet{
	public void init(){
		Editor editor = new Editor(this);
		editor.show();
	}
}

class Editor extends Frame implements MouseListener, MouseMotionListener, KeyListener, ActionListener
{
	private MenuItem makeMenuItem( String name ){
		MenuItem m = new MenuItem( name );
		m.addActionListener( this );
		return m;
	}

	// prepare the menus and start in title screen mode
	public Editor( Block2by2 p ){
		super("Editor");
		parent = p;
		setLayout( new BorderLayout() );
		Menu fileMenu = new Menu ("File");
		fileMenu.add( makeMenuItem("Load") );
		fileMenu.add( makeMenuItem("Save") );
		fileMenu.add( makeMenuItem("Quit") );
		Menu optionMenu = new Menu("Options");
		optionMenu.add( makeMenuItem("Load Standard Matrix") );
		optionMenu.add( makeMenuItem("Load Order 28 Matrix") );
		optionMenu.add( makeMenuItem("Player 1 Strategy") );
		optionMenu.add( makeMenuItem("Player 2 Strategy") );
		optionMenu.add( makeMenuItem("Timer On/Off") );
		MenuBar menuBar = new MenuBar();
		menuBar.add( fileMenu );
		menuBar.add( optionMenu );
		setMenuBar( menuBar );
		pack();

		this.setBackground(Color.white);
		setSize(640, 400); // the initial size of the window, it can be resized

		xOffset = 5;
		yOffset = 25;

		width = Math.min( getWidth()-15-2*xOffset, getHeight()-15-3*yOffset );
		height = width;

		titlescreen = true;

		addMouseListener(this);
		addMouseMotionListener(this);
		addKeyListener(this);
	}

	// count the scores and update the matrix that stores the block ranks
	// blue gets one point for every rank 1 block
	// green gets 1 point for every rank 2 block
	public void checkscore(){
		p1p = 0;
		p2p = 0;
		int sum = 0;
		for( int i = 0; i < N/2; i++ ){
			for( int j = 0; j < N/2; j++ ){
				sum = A[2 * i][2 * j] + A[2 * i][2 * j + 1] + A[2 * i + 1][2 * j] + A[2 * i + 1][2 * j + 1];
				if( sum == 2 || sum == -2 ){
					B[2 * i][2 * j] = 2;
					B[2 * i][2 * j + 1] = 2;
					B[2 * i + 1][2 * j] = 2;
					B[2 * i + 1][2 * j + 1] = 2;
					p2p ++;
				}else{
					B[2 * i][2 * j] = 1;
					B[2 * i][2 * j + 1] = 1;
					B[2 * i + 1][2 * j] = 1;
					B[2 * i + 1][2 * j + 1] = 1;
					p1p++;
				}
			}
		}
	}

	// some code here is awkward due to not having a separate panel for graphics
	// also, the title screen was hard-coded for my computer.
	public void paint(Graphics g){
		Graphics2D gui = (Graphics2D)g;

		// the menu takes about 45 pixels and the left bar takes about 7
		gui.translate(xOffset, yOffset);
		if( titlescreen ){
			gui.drawString("CLICK TO START", 250, 30 );

			gui.drawString("Controls:", 110, 60);
			gui.drawString("Up: Up Arrow", 110, 80);
			gui.drawString("Down: Down Arrow", 110, 95);
			gui.drawString("Left: Left Arrow", 110, 110);
			gui.drawString("Right: Right Arrow", 110, 125);
			gui.drawString("Select/Swap: Space", 110, 140);
			gui.drawString("Change Player: Q", 110, 155);

			gui.drawString("This program was designed to experiment with the 2-by-2 block conjecture.", 10,180);
			gui.drawString("The applet window displays the matrix you are currently working with.", 10,200);
			gui.drawString("Positive entries are represented by a vertical line, and negative entries are represented by a horizontal line.", 10,220);
			gui.drawString("Rank 1 blocks have a blue background and rank 2 blocks have a green background.", 10,240);
			gui.drawString("You manipulate the matrix by selecting two entries at a time. Once you have selected two entries,", 10,260);
			gui.drawString("	 the row and column of each entry will be switched with the row and column of the other entry.", 10,280);
		}else{
			//TODO - update the following code
			//updateImage();

			gui.drawImage( image, null, xOffset+15, yOffset+15 );

			// the menu and the bottom of the window take about 65 pixels
			// size is the size of each square in the matrix
			//size = (getHeight()-65)/N;
			size = (int)(image.getHeight()/N);

			gui.setColor(Color.black);
			// draw each player's scores
			gui.drawString("Player 1 Score: " + String.valueOf(p1p), size*N+10+15, 40+15);
			gui.drawString("Player 2 Score: " + String.valueOf(p2p), size*N+10+15, 60+15);
			// draw the time
			gui.drawString("Time: " + String.valueOf(time), size*N+10+15, 100+15);

			for( int i = 0; i < N; i++ ){
				gui.drawString(String.valueOf(colindex[i]), size*i+15+xOffset, 10+yOffset);
				gui.drawString(String.valueOf(rowindex[i]), xOffset, size*i+15+yOffset+size);
			}
			
			gui.drawString("Selected Player: " + String.valueOf(selectedPlayer), size*N+10+15,135);
			gui.drawString("P1 Automation: " + String.valueOf(p1a), size*N+10+15, 150);
			gui.drawString("P2 Automation: " + String.valueOf(p2a), size*N+10+15, 165);
			gui.drawString("Controls:", size*N+10+15, 50+180);
			gui.drawString("Up: Up Arrow", size*N+10+15, 50+195);
			gui.drawString("Down: Down Arrow", size*N+10+15, 50+210);
			gui.drawString("Left: Left Arrow", size*N+10+15, 50+225);
			gui.drawString("Right: Right Arrow", size*N+10+15, 50+240);
			gui.drawString("Select/Swap: Space", size*N+10+15, 50+255);
			gui.drawString("Change Player: Q", size*N+10+15, 50+270);
		}
	}

	public void updateImage(){
		width = Math.min( getWidth()-15-2*xOffset, getHeight()-15-3*yOffset );
		height = width;

		image = new BufferedImage( width, height, BufferedImage.TYPE_INT_ARGB );

		Graphics2D gui = (Graphics2D)image.getGraphics();

		size = (int)(image.getHeight()/N);

		// draw each square in the colour of its owner
		for( int i = 0; i < N; i++ ){
			for( int j = 0; j < N; j++ ){
				if( B[i][j] == 1 ){
					gui.setColor(Color.blue);
					gui.fill(new Rectangle2D.Double(j * size, i * size, size, size) );
				}
				if( B[i][j] == 2 ){
					gui.setColor(Color.green);
					gui.fill(new Rectangle2D.Double(j * size, i * size, size, size) );
				}
			}
		}

		// draw the players' markers
		// if either player is computer-controlled, draw the destination square
		gui.setColor(Color.white);
		if( p1a == 1 ){
			gui.setColor(Color.red);
			gui.fill(new Rectangle2D.Double(p1destc * size + size / 4, p1destr * size + size / 4, size/2, size/2) );
		}
		if( p2a == 1 ){
			gui.setColor(Color.magenta);
			gui.fill(new Rectangle2D.Double(p2destc * size + size / 4, p2destr * size + size / 4, size/2, size/2) );
		}

		// now draw each player's first marker
		gui.setColor(Color.white);
		gui.fill(new Rectangle2D.Double(p1c1 * size + 1, p1r1 * size + 1, size-2, size-2) );
		gui.fill(new Rectangle2D.Double(p2c1 * size + 1, p2r1 * size + 1, size-2, size-2) );
		gui.setColor(Color.blue);
		gui.fill(new Rectangle2D.Double(p1c1 * size + size / 4, p1r1 * size + size / 4, size/2, size/2) );
		gui.setColor(Color.green);
		gui.fill(new Rectangle2D.Double(p2c1 * size + size / 4, p2r1 * size + size / 4, size/2, size/2) );

		// now draw each player's second marker, if it is active
		if( p1m == 1 ){
			gui.setColor(Color.blue);
			gui.fill(new Rectangle2D.Double(p1c2 * size + 1, p1r2 * size + 1, size-2, size-2) );
			gui.setColor(Color.white);
			gui.fill(new Rectangle2D.Double(p1c2 * size + size / 4, p1r2 * size + size / 4, size/2, size/2) );
		}
		if( p2m == 1 ){
			gui.setColor(Color.green);
			gui.fill(new Rectangle2D.Double(p2c2 * size + 1, p2r2 * size + 1, size-2, size-2) );
			gui.setColor(Color.white);
			gui.fill(new Rectangle2D.Double(p2c2 * size + size / 4, p2r2 * size + size / 4, size/2, size/2) );
		}

		// now draw the lines representing the value of each entry in the matrix
		gui.setColor(Color.black);
		for( int i = 0; i < N; i++ ){
			for( int j = 0; j < N; j++ ){
				if( A[i][j] == 1 ){
					gui.drawString("1", (j * size) + (int)(size/2), (i * size) + (int)(size/2));
					//gui.fill(new Rectangle2D.Double(j * size + size / 2 - 1, i * size, 3, size) );
				}
				if( A[i][j] == -1 ){
					gui.drawString("-", (j * size) + (int)(size/2), (i * size) + (int)(size/2));
					//gui.fill(new Rectangle2D.Double(j * size, i * size + size / 2 - 1, size, 3) );
				}
			}
		}
	}
	
	public void InitScreen(){
		titlescreen = false;

		// set up the timer
		int delay = 4; // set this higher to slow the game down
		timer = new Timer( delay, this );
		timer.setInitialDelay(0);
		timer.setCoalesce(true);
		timer.start();
		usingTimer = false;

		rnd = new Random();

		// I should give some of these variables more useful names.

		filename = "had.24.1.txt";
		board = 1;
		p1r1 = 0;
		p1c1 = 0;
		p1r2 = 0;
		p1c2 = 0;
		p1p = 0;
		p1m = 0;
		p2r1 = N-1;
		p2c1 = N-1;
		p2r2 = N-1;
		p2c2 = N-1;
		p2p = 0;
		p2m = 0;
		refresh = 1;
		size = 400 / N;

		p1a = 0;
		p1movecheat = 1;
		p1move = 1000 / 1000;
		p1choose = 1000 / 1000;
		p1think = 1000 / 1000;
		p1strategy = 4;

		p1cyclenum = 0;
		p1destr = 1;
		p1destc = 1;
		p1ready = 0;
		p1go = 0;

		p2a = 0;
		p2movecheat = 1;
		p2move = 1000 / 1000;
		p2choose = 1000 / 1000;
		p2think = 1000 / 1000;
		p2strategy = 2;

		p2cyclenum = 0;
		p2destr = 1;
		p2destc = 1;
		p2ready = 0;
		p2go = 0;

		time = 0;

		A = new int[N][N];
		B = new int[N][N];

		String line;
		URL url=null;

		// on startup, load the order 24 paley matrix
		try{
			url = new URL (parent.getCodeBase(), "had.24.1.txt" );
		}
		catch (MalformedURLException e ){
			System.out.println("Malformed URL ");
			//stop();
		}

		try{
			InputStream in=url.openStream();
			reader = new BufferedReader(new InputStreamReader(in));

			openFile( reader );

			in.close();
		}
		catch (IOException e ){
		}

		checkscore();

		// true iff the button is being pressed
		kj = false;
		kk = false;
		kj = false;
		kl = false;
		kf = false;
		kd = false;

		k8 = false;
		k5 = false;
		k4 = false;
		k6 = false;
		kp = false;
		k0 = false;
		updateImage();
		repaint();
	}

	
	public void mouseClicked(MouseEvent event)
	{
		if( titlescreen ){
			titlescreen = false;

			// set up the timer
			int delay = 40; // set this higher to slow the game down
			timer = new Timer( delay, this );
			timer.setInitialDelay(0);
			timer.setCoalesce(true);
			timer.start();
			usingTimer = false;

			rnd = new Random();

			// I should give some of these variables more useful names.

			filename = "had.24.1.txt";
			board = 1;
			p1r1 = 0;
			p1c1 = 0;
			p1r2 = 0;
			p1c2 = 0;
			p1p = 0;
			p1m = 0;
			p2r1 = N-1;
			p2c1 = N-1;
			p2r2 = N-1;
			p2c2 = N-1;
			p2p = 0;
			p2m = 0;
			refresh = 1;
			size = 400 / N;

			p1a = 0;
			p1movecheat = 1;
			p1move = 1000 / 1000;
			p1choose = 1000 / 1000;
			p1think = 1000 / 1000;
			p1strategy = 4;

			p1cyclenum = 0;
			p1destr = 1;
			p1destc = 1;
			p1ready = 0;
			p1go = 0;

			p2a = 0;
			p2movecheat = 1;
			p2move = 1000 / 1000;
			p2choose = 1000 / 1000;
			p2think = 1000 / 1000;
			p2strategy = 2;

			p2cyclenum = 0;
			p2destr = 1;
			p2destc = 1;
			p2ready = 0;
			p2go = 0;

			time = 0;

			A = new int[N][N];
			B = new int[N][N];

			String line;
			URL url=null;

			// on startup, load the order 24 paley matrix
			try{
				url = new URL (parent.getCodeBase(), "had.24.1.txt" );
			}
			catch (MalformedURLException e ){
				System.out.println("Malformed URL ");
				//stop();
			}

			try{
				InputStream in=url.openStream();
				reader = new BufferedReader(new InputStreamReader(in));

				openFile( reader );

				in.close();
			}
			catch (IOException e ){
			}

			checkscore();

			// true iff the button is being pressed
			kj = false;
			kk = false;
			kj = false;
			kl = false;
			kf = false;
			kd = false;

			k8 = false;
			k5 = false;
			k4 = false;
			k6 = false;
			kp = false;
			k0 = false;
			updateImage();
			repaint();
		}else{
			// mouse control isn't implemented yet

			int mouseX = event.getX();
			int mouseY = event.getY();

			//		p1X = mouseX;
			//		p1Y = mouseY;
			updateImage();
			repaint();
		}
	}
	

	public void mousePressed(MouseEvent event){}

	public void mouseReleased(MouseEvent event){}

	public void mouseDragged(MouseEvent event){}

	public void mouseEntered(MouseEvent event){}

	public void mouseExited(MouseEvent event){}

	public void mouseMoved(MouseEvent event){}

	public void keyTyped(KeyEvent e){}

	public void keyPressed(KeyEvent e){
		int keyCode = e.getKeyCode();
		boolean curBool = true;

		switch (keyCode){
		case KeyEvent.VK_UP:
			if (selectedPlayer == 1 && p1a != 1)
				ki = curBool;
			else if (selectedPlayer == 2 && p2a != 1)
				k8 = curBool;
			break;
		case KeyEvent.VK_DOWN:
			if (selectedPlayer == 1 && p1a != 1)
				kk = curBool;
			else if (selectedPlayer == 2 && p2a != 1)
				k5 = curBool;
			break;
		case KeyEvent.VK_LEFT:
			if (selectedPlayer == 1 && p1a != 1)
				kj = curBool;
			else if (selectedPlayer == 2 && p2a != 1)
				k4 = curBool;
			break;
		case KeyEvent.VK_RIGHT:
			if (selectedPlayer == 1 && p1a != 1)
				kl = curBool;
			else if (selectedPlayer == 2 && p2a != 1)
				k6 = curBool;
			break;
		case KeyEvent.VK_SPACE:
			if (selectedPlayer == 1 && p1a != 1)
				kf = curBool;
			else if (selectedPlayer == 2 && p2a != 1)
				k0 = curBool;
			break;
		case KeyEvent.VK_Z:
			if (selectedPlayer == 1 && p1a != 1)
				kd = curBool;
			else if (selectedPlayer == 2 && p2a != 1)
				kp = curBool;
			break;
		}
		keyPressed = true;
	}

	public void keyReleased(KeyEvent e){
		int keyCode = e.getKeyCode();
		boolean curBool = false;

		switch (keyCode){
		case KeyEvent.VK_UP:
			if (selectedPlayer == 1 && p1a != 1)
				ki = curBool;
			else if (selectedPlayer == 2 && p2a != 1)
				k8 = curBool;
			break;
		case KeyEvent.VK_DOWN:
			if (selectedPlayer == 1 && p1a != 1)
				kk = curBool;
			else if (selectedPlayer == 2 && p2a != 1)
				k5 = curBool;
			break;
		case KeyEvent.VK_LEFT:
			if (selectedPlayer == 1 && p1a != 1)
				kj = curBool;
			else if (selectedPlayer == 2 && p2a != 1)
				k4 = curBool;
			break;
		case KeyEvent.VK_RIGHT:
			if (selectedPlayer == 1 && p1a != 1)
				kl = curBool;
			else if (selectedPlayer == 2 && p2a != 1)
				k6 = curBool;
			break;
		case KeyEvent.VK_SPACE:
			if (selectedPlayer == 1 && p1a != 1)
				kf = curBool;
			else if (selectedPlayer == 2 && p2a != 1)
				k0 = curBool;
			break;
		case KeyEvent.VK_Z:
			if (selectedPlayer == 1 && p1a != 1)
				kd = curBool;
			else if (selectedPlayer == 2 && p2a != 1)
				kp = curBool;
			break;
		case KeyEvent.VK_Q:
			if (selectedPlayer == 1 && p1a != 1)
				selectedPlayer = 2;
			else if (selectedPlayer == 2 && p2a != 1)
				selectedPlayer = 1;
			break;
		case KeyEvent.VK_S:
			p2strategy = 3;
			usingTimer = !usingTimer;
			break;
		}
		updateImage();
		repaint();
	}


	// loads a file the user chooses from a dialog, doesn't work in an applet
	private void loadFile ()
	{
		FileDialog fd = new FileDialog( this, "Load File", FileDialog.LOAD );
		fd.show();

		/*
	String file = fd.getDirectory() + fd.getFile();
	if ( file == null ) // Cancel
	  return;
	System.out.println("filename is " + file );
		 */

		String file = parent.getCodeBase() + fd.getFile();
		if ( file == null ) // Cancel
			return;
		System.out.println("urlname is " + file );

		board = 1;
		A = new int[N][N];
		B = new int[N][N];

		String line;
		URL url=null;

		try
		{
			url = new URL (parent.getCodeBase(), fd.getFile() );
		}
		catch (MalformedURLException  e )
		{
			System.out.println("Malformed URL ");
			//stop();
		}

		try
		{
			InputStream in=url.openStream();
			reader = new BufferedReader(new InputStreamReader(in));

			openFile( reader );

			in.close();
		}

		/*
	try
	{
	  FileInputStream in = new FileInputStream ( file );
	  reader = new BufferedReader(new InputStreamReader(in));

	  A = new int[N][N];

	  openFile(reader);

	  in.close();
	}
		 */
		catch ( IOException e )
		{
			System.out.println( "Could not load file..." );
		}
	}

	//		double tempMinRealPart = Double.parseDouble( JOptionPane.showInputDialog(this, null, "input real lower bound", 0) );


	// load the matrix in the file specified by the URL
	//  this file may denote the entries in the matrix with any combination of
	//  1's, 0's, -1's, +'s, and -'s
	//  newlines do not affect this function,
	//  and neither do lines containing other text
	// The size N of the matrix needs to have already been set
	void openFile( BufferedReader reader )
	{
		StringTokenizer tokens;
		String token = "";

		String line = "";
		int i = 0;
		int j = 0;
		int k = 0;
		// repeat until the whole matrix was read or until end of file
		while( i < N ) //&& !EOF(reader) )
		{
			line = readLine(reader);

			tokens = new StringTokenizer( line );
			if( tokens.hasMoreTokens() )
				if( tokens.nextToken().equals("size") )
				{
					N = readInt(reader);

					A = new int[N][N];
					B = new int[N][N];

					line = readLine(reader);
				}

			k = 0;
			tokens = new StringTokenizer( line );
			// process all tokens in the current line
			while( j < N && tokens.hasMoreTokens() && i < N )
			{
				//System.out.println( "" + i + " " + j );
				token = tokens.nextToken();
				// these tokens represent a 1 entry
				if( token.equals("1") || token.equals("+") )
				{
					A[i][j] = 1;
					j++;
					if( j == N )
					{
						i++;
						j = 0;
					}
				}
				// these tokens represent a -1 entry
				else if( token.equals("0") || token.equals("-1") || token.equals("-") )
				{
					A[i][j] = -1;
					j++;
					if( j == N )
					{
						i++;
						j = 0;
					}
				}
				// if an illegal token is detected, read the token character by character
				else
				{
					k = 0;
					while( k >= 0 && j < N && k < token.length() && i < N )
					{
						// these characters represent a 1 entry
						if( token.charAt(k) == '1' || token.charAt(k) == '+' )
						{
							A[i][j] = 1;
							j++;
							if( j == N )
							{
								i++;
								j = 0;
							}
						}
						// these characters represent a -1 entry
						else if( token.charAt(k) == '0' || token.charAt(k) == '-' )
						{
							A[i][j] = -1;
							j++;
							if( j == N )
							{
								i++;
								j = 0;
							}
						}
						// if the character is invalid, then move to the next line
						else
						{
							k = -10;
						}
						k++;
					}
					line = readLine(reader);
					if( line != null )
						tokens = new StringTokenizer( line );
				}
			}
		}

		int y = 0;
		int z = 0;

		// now check if the matrix is hadamard
		z = 0;
		for( i = 0; i < N; i++ )
			for( j = 0; j < N; j++ )
				if( i != j )
				{
					y = 0;
					for( k = 0; k < N; k++ )
						y += A[i][k] * A[j][k];
					z -= Math.abs(y);
				}
		if( z != 0 )
			System.out.println( "This is not a Hadamard matrix!" );

		// now convert the matrix to standard form, in which the first row
		//	and first column are all positive
		// do this by inverting the rows and columns that start with a -1
		for( i = 0; i < N; i++ )
		{
			if( A[0][i] == -1 )
				for( j = 0; j < N; j++ )
					A[j][i] *= -1;
			if( A[i][0] == -1 )
				for( j = 0; j < N; j++ )
					A[i][j] *= -1;
		}

		// initialize all of the arrays to their new size
		rowcurrpos = new int[N];
		colcurrpos = new int[N];

		poscolindex = new int[N];
		negcolindex = new int[N];
		poscolused = new int[N];
		negcolused = new int[N];

		rowindex = new int[N];
		colindex = new int[N];
		nextposcol = new int[N];
		nextnegcol = new int[N];
		leftposcol = new int[N];
		leftnegcol = new int[N];
		sizes = new int[N][N];

		colused = new int[N];
		nextcol = new int[N];
		leftcol = new int[N];
		binaryval = new int[N];
		otherbinaryval = new int[N];
		tag = new int[N];


		// initialize all the variables
		sortingrows = false;
		rowsortloop2 = false;
		previousswap = false;

		numposcols = 0;
		numnegcols = 0;
		numposlevels = 0;
		numneglevels = 0;

		level = 0;
		firstchoice = -1;
		nextfirstchoice = -1;
		nextnextposcol = -1;
		nextnextnegcol = -1;
		groupnum = 0;
		ourgroupnum = 0;
		groupsize = 0;
		done = false;
		success = false;

		numdiffs = 0;
		numdiffs2 = 0;
		groupstart = 0;
		groupend = N-1;
		goback = false;
		firstiteration = true;
		finishedpopping = true;

		nextnextcol = -1;

		// now find and store the positive and negative columns
		for( i = 1; i < N; i++ )
		{
			if( A[1][i] == 1 )
			{
				poscolindex[numposcols] = i;
				numposcols++;
			}
			if( A[1][i] == -1 )
			{
				negcolindex[numnegcols] = i;
				numnegcols++;
			}
		}
		numposlevels = (numposcols-1)/2;
		numneglevels = numnegcols/2;
		numlevels = numposlevels + numneglevels;

		// now initialize the other arrays to their default values
		for( i = 0; i < N; i++ ){
			poscolused[i] = 0;
			negcolused[i] = 0;
			nextposcol[i] = -1;
			nextnegcol[i] = -1;
			leftposcol[i] = -1;
			leftnegcol[i] = -1;
			rowindex[i] = i;
			colindex[i] = i;
			rowcurrpos[i] = i;
			colcurrpos[i] = i;
			for( j = 2; j < N; j++ )
				sizes[i][j] = 0;

			colused[i] = 0;
			nextcol[i] = -1;
			leftcol[i] = -1;
			binaryval[i] = 0;
			otherbinaryval[i] = 0;
			tag[i] = i;
		}

		checkscore();
		updateImage();
		repaint();
	}

	// save the current data to a file, using the +/- format, with no spaces
	// doesn't work in an applet
	private void saveFile(){
		int h;
		int i;
		int j;

		BufferedWriter writer;
		FileDialog fd = new FileDialog( this, "Save File", FileDialog.SAVE );
		fd.show();
		// Save file data...

		String file = fd.getDirectory() + fd.getFile();
		if ( file == null ) // Cancel
			return;
		//	  System.out.println("filename is " + file );
		String outputLine = "";

		try
		{
			FileOutputStream out = new FileOutputStream ( file );
			writer = new BufferedWriter( new OutputStreamWriter(out) );

			// indicate that the size is known
			outputLine = "size";
			writer.write(outputLine, 0, outputLine.length() );
			writer.write("\n", 0, 1 );

			// write the size of the matrix
			outputLine = String.valueOf(N);
			writer.write(outputLine, 0, outputLine.length() );
			writer.write("\n", 0, 1 );

			// write the matrix
			for( i = 0; i < N; i++ ){
				outputLine = "";

				for( j = 0; j < N; j++ ){
					if( A[i][j] == 1 )
						outputLine += "+";
					if( A[i][j] == -1 )
						outputLine += "-";
				}

				writer.write(outputLine, 0, outputLine.length() );
				writer.write("\n", 0, 1 );
			}

			writer.flush();
			out.close();

			//		updateImage();
			repaint();
			//		System.out.println("File saveed successfully.");

		}
		catch ( IOException e ){
			System.out.println("Could not save file...");
		}

	}

	// called when the timer ticks or a menu item is chosen
	public void actionPerformed(ActionEvent e){
		// first check if the action performed was a menu selection
		String command = "";
		if( e != null )
			command = e.getActionCommand();
		if( command == null )
			command = "";
		if ( command.equals("Quit") ){
			dispose();
			updateImage();
			repaint();
			return;
		}
		else if ( command.equals("Load") ){
			loadFile();
			updateImage();
			repaint();
			return;
		}
		else if ( command.equals("Save") ){
			saveFile();
			updateImage();
			repaint();
			return;
		}
		// prompts for board size, then board number, and loads the specified board
		else if ( command.equals("Load Standard Matrix") ){
			String nameString = "";
			Object[] possibilities =
				{
					"had.8.txt",
					"had.8.1.txt",
					"had.12.txt",
					"had.16.0.txt",
					"had.16.hed.txt",
					"had.16.syl.txt",
					"had.16.twin.txt",
					"had.16.1.txt",
					"had.16.2.txt",
					"had.16.3.txt",
					"had.16.4.txt",
					"had.20.hall.n.txt",
					"had.20.julin.txt",
					"had.20.pal.txt",
					"had.20.toncheviii.txt",
					"had.20.toncheviv.txt",
					"had.20.will.txt",
					"had.20.1.txt",
					"had.20.2.txt",
					"had.20.3.txt",
					"had.24.pal.txt",
					"had.24.1.txt",
					"had.24.2.txt",
					"had.24.3.txt",
					"had.24.4.txt",
					"had.24.5.txt",
					"had.24.6.txt",
					"had.24.7.txt",
					"had.24.8.txt",
					"had.24.9.txt",
					"had.24.10.txt",
					"had.24.11.txt",
					"had.24.12.txt",
					"had.24.13.txt",
					"had.24.14.txt",
					"had.24.15.txt",
					"had.24.16.txt",
					"had.24.17.txt",
					"had.24.18.txt",
					"had.24.19.txt",
					"had.24.20.txt",
					"had.24.21.txt",
					"had.24.22.txt",
					"had.24.23.txt",
					"had.24.24.txt",
					"had.24.25.txt",
					"had.24.26.txt",
					"had.24.27.txt",
					"had.24.28.txt",
					"had.24.29.txt",
					"had.24.30.txt",
					"had.24.31.txt",
					"had.24.32.txt",
					"had.24.33.txt",
					"had.24.34.txt",
					"had.24.35.txt",
					"had.24.36.txt",
					"had.24.37.txt",
					"had.24.38.txt",
					"had.24.39.txt",
					"had.24.40.txt",
					"had.24.41.txt",
					"had.24.42.txt",
					"had.24.43.txt",
					"had.24.44.txt",
					"had.24.45.txt",
					"had.24.46.txt",
					"had.24.47.txt",
					"had.24.48.txt",
					"had.24.49.txt",
					"had.24.50.txt",
					"had.24.51.txt",
					"had.24.52.txt",
					"had.24.53.txt",
					"had.24.54.txt",
					"had.24.55.txt",
					"had.24.56.txt",
					"had.24.57.txt",
					"had.24.58.txt",
					"had.24.59.txt",
					"had.24.60.txt",
					"had.28.pal2.txt",
					"had.28.will.txt",
					"had.32.pal.txt",
					"had.32.syl.txt",
					"had.32.t1.txt",
					"had.32.t2.txt",
					"had.32.t3.txt",
					"had.32.t4.txt",
					"had.36.pal2.txt",
					"had.36.will.txt",
					"had.40.tpal.txt",
					"had.40.ttoncheviv.txt",
					"had.40.twill.txt",
					"had.44.pal.txt",
					"had.44.will.txt",
					"had.48.pal.txt",
					"had.52.will.txt",
					"had.56.tpal2.txt",
					"had.56.twill.txt",
					"had.60.pal.txt",
					"had.60.pal2.txt",
					"had.60.will.txt",
					"had.64.syl.txt",
					"had.68.pal.txt",
					"had.68.will.txt",
					"had.72.pal.txt",
					"had.76.pal2.txt",
					"had.76.will.txt",
					"had.80.pal.txt",
					"had.84.pal.txt",
					"had.84.pal2.txt",
					"had.84.will.txt",
					"had.88.tpal.txt",
					"had.88.twill.txt",
					"had.92.will.txt",
					"had.96.tpal.txt",
					"had.100.will.txt",
					"had.104.pal.txt",
					"had.108.pal.txt",
					"had.108.pal2.txt",
					"had.108.will.txt",
					"had.112.ttpal2.txt",
					"had.112.ttwill.txt",
					"had.116.will.txt",
					"had.120.tpal.txt",
					"had.120.tpal2.txt",
					"had.120.twill.txt",
					"had.124.pal2.txt",
					"had.124.will.txt",
					"had.128.syl.txt",
					"had.132.pal.txt",
					"had.132.will.txt",
					"had.136.tpal.txt",
					"had.136.twill.txt",
					"had.140.pal.txt",
					"had.144.tpal.txt",
					"had.148.pal2.txt",
					"had.148.will.txt",
					"had.152.pal.txt",
					"had.156.will.txt",
					"had.160.tpal.txt",
					"had.164.pal.txt",
					"had.168.pal.txt",
					"had.172.will.txt",
					"had.176.ttpal.txt",
					"had.176.ttwill.txt",
					"had.180.pal.txt",
					"had.184.twill.txt",
					"had.188.tur.txt",
					"had.192.pal.txt",
					"had.196.pal2.txt",
					"had.200.pal.txt",
					"had.204.pal2.txt",
					"had.208.twill.txt",
					"had.212.pal.txt",
					"had.216.tpal.txt",
					"had.220.pal2.txt",
					"had.224.pal.txt",
					"had.228.pal.txt",
					"had.232.twill.txt",
					"had.236.od.txt",
					"had.240.pal.txt",
					"had.244.will.txt",
					"had.248.twill.txt",
					"had.252.pal.txt",
					"had.256.syl.txt"
				};
			nameString = (String)JOptionPane1.showInputDialog(
					this,
					"Choose a matrix:\n",
					"Choose a Matrix",
					JOptionPane.PLAIN_MESSAGE,
					null,
					possibilities,
					"had.8.txt" );

			if ((nameString != null) && (nameString.length() > 0))
			{
				if( nameString.charAt(5) == '.' )
					N = Integer.parseInt(nameString.substring(4, 5) );
				else if( nameString.charAt(6) == '.' )
					N = Integer.parseInt(nameString.substring(4, 6) );
				else if( nameString.charAt(7) == '.' )
					N = Integer.parseInt(nameString.substring(4, 7) );

				A = new int[N][N];
				B = new int[N][N];

				URL url = null;
				try
				{
					url = new URL (parent.getCodeBase(), nameString );
					//url = new URL ( "http://www.research.att.com/~njas/hadamard/" + nameString ); // this didn't work
				}
				catch (MalformedURLException  e2 )
				{
					System.out.println("Malformed URL ");
				}

				try
				{
					InputStream in=url.openStream();
					reader = new BufferedReader(new InputStreamReader(in));

					openFile( reader );

					in.close();
				}
				catch (IOException e3 )
				{
				}
			}
			p1r1 = 0;
			p1c1 = 0;
			p1r2 = 0;
			p1c2 = 0;
			p1p = 0;
			p1m = 0;
			p2r1 = N-1;
			p2c1 = N-1;
			p2r2 = N-1;
			p2c2 = N-1;
			p2p = 0;
			p2m = 0;
			refresh = 1;
			size = 400 / N;

			p1a = 0;
			p1movecheat = 1;
			p1move = 1000 / 1000;
			p1choose = 1000 / 1000;
			p1think = 1000 / 1000;
			p1strategy = 4;

			p1cyclenum = 0;
			p1destr = 1;
			p1destc = 1;
			p1ready = 0;
			p1go = 0;

			p2a = 0;
			p2movecheat = 1;
			p2move = 1000 / 1000;
			p2choose = 1000 / 1000;
			p2think = 1000 / 1000;
			p2strategy = 2;

			p2cyclenum = 0;
			p2destr = 1;
			p2destc = 1;
			p2ready = 0;
			p2go = 0;

			time = 0;

			checkscore();
			updateImage();
			repaint();
			return;
		}
		else if ( command.equals("Load Order 28 Matrix") )
		{
			String nameString = "Had.28." + JOptionPane.showInputDialog(this, null, "input matrix number", 0);

			if ((nameString != null) && (nameString.length() > 0))
			{
				if( nameString.charAt(5) == '.' )
					N = Integer.parseInt(nameString.substring(4, 5) );
				else if( nameString.charAt(6) == '.' )
					N = Integer.parseInt(nameString.substring(4, 6) );
				else if( nameString.charAt(7) == '.' )
					N = Integer.parseInt(nameString.substring(4, 7) );

				A = new int[N][N];
				B = new int[N][N];

				URL url = null;
				try
				{
					url = new URL (parent.getCodeBase(), nameString );
					//url = new URL ( "http://www.research.att.com/~njas/hadamard/" + nameString ); // this didn't work
				}
				catch (MalformedURLException  e2 )
				{
					System.out.println("Malformed URL ");
				}

				try
				{
					InputStream in=url.openStream();
					reader = new BufferedReader(new InputStreamReader(in));

					openFile( reader );

					in.close();
				}
				catch (IOException e3 )
				{
				}
			}
			p1r1 = 0;
			p1c1 = 0;
			p1r2 = 0;
			p1c2 = 0;
			p1p = 0;
			p1m = 0;
			p2r1 = N-1;
			p2c1 = N-1;
			p2r2 = N-1;
			p2c2 = N-1;
			p2p = 0;
			p2m = 0;
			refresh = 1;
			size = 400 / N;

			p1a = 0;
			p1movecheat = 1;
			p1move = 1000 / 1000;
			p1choose = 1000 / 1000;
			p1think = 1000 / 1000;
			p1strategy = 4;

			p1cyclenum = 0;
			p1destr = 1;
			p1destc = 1;
			p1ready = 0;
			p1go = 0;

			p2a = 0;
			p2movecheat = 1;
			p2move = 1000 / 1000;
			p2choose = 1000 / 1000;
			p2think = 1000 / 1000;
			p2strategy = 2;

			p2cyclenum = 0;
			p2destr = 1;
			p2destc = 1;
			p2ready = 0;
			p2go = 0;

			time = 0;

			checkscore();
			updateImage();
			repaint();
			return;

		}
		// prompts for player 1's strategy, then sets it
		else if ( command.equals("Player 1 Strategy") )
		{
			String choiceString = "";
			Object[] possibilities =
				{
					"keyboard control",
					"random 1",
					"random 2",
					"heuristic 1",
					"improved heuristic 1",
					"exhaustive search",
					"fast random 2",
					"fast heuristic 1",
					"fast improved heuristic 1",
					"fast exhaustive search",
					"faster exhaustive search",
					"fastest exhaustive search"
				};
			choiceString = (String)JOptionPane1.showInputDialog(
					this,
					"Choose player 1's strategy:\n",
					"Choose a Strategy",
					JOptionPane.PLAIN_MESSAGE,
					null,
					possibilities,
					"keyboard control" );

			if ((choiceString != null) && (choiceString.length() > 0)){
				if( choiceString.equals("keyboard control") ){
					p1m = 0;
					p1ready = 0;
					p1go = 0;
					p1a = 0;
					p1movecheat = 0;
					p1strategy = 0;
				}
				if( choiceString.equals("random 1") ){
					p1ready = 0;
					p1a = 1;
					p1movecheat = 0;
					p1strategy = 1;
				}
				if( choiceString.equals("random 2") ){
					p1ready = 0;
					p1a = 1;
					p1movecheat = 0;
					p1strategy = 2;
				}
				if( choiceString.equals("heuristic 1") ){
					p1ready = 0;
					p1a = 1;
					p1movecheat = 0;
					p1strategy = 3;
				}
				if( choiceString.equals("improved heuristic 1") ){
					p1ready = 0;
					p1a = 1;
					p1movecheat = 0;
					p1strategy = 4;
				}
				if( choiceString.equals("exhaustive search") ){
					p1ready = 0;
					p1a = 1;
					p1movecheat = 0;
					p1strategy = 5;
				}
				if( choiceString.equals("fast random 2") ){
					p1ready = 0;
					p1a = 1;
					p1movecheat = 1;
					p1strategy = 2;
				}
				if( choiceString.equals("fast heuristic 1") ){
					p1ready = 0;
					p1a = 1;
					p1movecheat = 1;
					p1strategy = 3;
				}
				if( choiceString.equals("fast improved heuristic 1") ){
					p1ready = 0;
					p1a = 1;
					p1movecheat = 1;
					p1strategy = 4;
				}
				if( choiceString.equals("fast exhaustive search") ){
					p1ready = 0;
					p1a = 1;
					p1movecheat = 1;
					p1strategy = 5;
				}
				if( choiceString.equals("faster exhaustive search") ){
					p1ready = 0;
					p1a = 1;
					p1movecheat = 1;
					p1strategy = 6;
				}
				if( choiceString.equals("fastest exhaustive search") ){
					p1ready = 0;
					p1a = 1;
					p1movecheat = 1;
					p1strategy = 7;
				}
			}
			return;
		}

		// prompts for player 2's strategy, then sets it
		else if ( command.equals("Player 2 Strategy") ){
			String choiceString = "";
			Object[] possibilities =
				{
					"keyboard control",
					"random 1",
					"random 2",
					"heuristic 1",
					"improved heuristic 1",
					"fast random 2",
					"fast heuristic 1",
					"fast improved heuristic 1",
					"faster exhaustive search",
					"fastest exhaustive search"
				};
			choiceString = (String)JOptionPane1.showInputDialog(
					this,
					"Choose player 2's strategy:\n",
					"Choose a Strategy",
					JOptionPane.PLAIN_MESSAGE,
					null,
					possibilities,
					"keyboard control" );

			if ((choiceString != null) && (choiceString.length() > 0)){
				if( choiceString.equals("keyboard control") ){
					p2m = 0;
					p2ready = 0;
					p2go = 0;
					p2a = 0;
					p2movecheat = 0;
					p2strategy = 0;
				}
				if( choiceString.equals("random 1") ){
					p2ready = 0;
					p2a = 1;
					p2movecheat = 0;
					p2strategy = 1;
				}
				if( choiceString.equals("random 2") ){
					p2ready = 0;
					p2a = 1;
					p2movecheat = 0;
					p2strategy = 2;
				}
				if( choiceString.equals("heuristic 1") ){
					p2ready = 0;
					p2a = 1;
					p2movecheat = 0;
					p2strategy = 3;
				}
				if( choiceString.equals("improved heuristic 1") ){
					p2ready = 0;
					p2a = 1;
					p2movecheat = 0;
					p2strategy = 4;
				}
				if( choiceString.equals("fast random 2") ){
					p2ready = 0;
					p2a = 1;
					p2movecheat = 1;
					p2strategy = 2;
				}
				if( choiceString.equals("fast heuristic 1") ){
					p2ready = 0;
					p2a = 1;
					p2movecheat = 1;
					p2strategy = 3;
				}
				if( choiceString.equals("fast improved heuristic 1") ){
					p2ready = 0;
					p2a = 1;
					p2movecheat = 1;
					p2strategy = 4;
				}
				if( choiceString.equals("faster exhaustive search") ){
					p2ready = 0;
					p2a = 1;
					p2movecheat = 1;
					p2strategy = 6;
				}
				if( choiceString.equals("fastest exhaustive search") ){
					p2ready = 0;
					p2a = 1;
					p2movecheat = 1;
					p2strategy = 7;
				}
			}
			return;
		}
		else if ( command.equals("Timer On/Off") )
		{
			usingTimer = !usingTimer;
			return;
		}


		// if it wasn't a menu item, assume it was a timer tick

		// perform the actions only if there was a real or fake timer tick
		if( usingTimer || keyPressed )
		{

			// unused code segment that cuts a computer player's speed in half
			//  if it is losing.	not converted to java yet
			/*
		time = time + 1
		if( time % 1000 = 0 && 1 = 0 )
		  if( p1p > p2p && p1move >= p2move )
			p2move = p2move * 2
			p2choose = p2choose * 2
			p2think = p2think * 2
		  }
		  if( p1p < p2p && p1move <= p2move )
			p1move = p1move * 2
			p1choose = p1choose * 2
			p1think = p1think * 2
		  }
		}
			 */

			// unimplemented shortcut keys for toggling automatic control
			//  if( A$ = "Z" ) p1a = 0
			//  if( A$ = "X" ) p1a = 1
			//  if( A$ = "C" ) p2a = 0
			//  if( A$ = "V" ) p2a = 1


			// strategy 1 just presses a random button
			if( p1strategy == 1 && p1a == 1 )
			{
				if( rnd.nextInt(p1move) == 0 )
				{
					kj = false;
					kk = false;
					kj = false;
					kl = false;
					kf = false;
					kd = false;

					int t = rnd.nextInt(5);
					switch(t)
					{
					case 0:
						ki = true;
						break;
					case 1:
						kk = true;
						break;
					case 2:
						kj = true;
						break;
					case 3:
						kl = true;
						break;
					case 4:
						kf = true;
						break;
					}
				}
			}

			// strategy 2 is to move to a random destination, then choose it
			if( p1strategy == 2 && p1a == 1 )
			{
				if( p1go == 0 )
				{
					p1destr = rnd.nextInt(N);
					p1destc = rnd.nextInt(N);
					p1go = 1;
				}
			}

			// strategy 3 and 4 are to find the two blocks whose rows and columns have
			//	the lowest score, then switch them
			// strategy 4 also checks which elements in the block would be best to switch

			if( ( p1strategy == 3 || p1strategy == 4 ) && p1a == 1 )
			{
				// temporary variables
				int score = 0;
				int otherp1row1 = 0;
				int otherp1col1 = 0;
				int temp = 0;
				int maxtp1p = 0;
				int maxg = 0;
				int maxh = 0;
				int g = 0;
				int h = 0;
				int tp1p = 0;
				int tp2p = 0;
				int sum = 0;

				if( rnd.nextInt(p1think) == 0 && p1ready == 0 && p1go == 0 )
				{
					p1row1 = 0;
					p1rowblock1 = 0;
					row1score = 0;
					// search each row of blocks
					for( int i = 0; i < N/2; i++ )
					{
						score = 0;
						// check each block in that row
						for( int j = 0; j < N/2; j++ )
						{
							// score one point for each block owned by the other player
							if( B[i * 2][j * 2] == 2 )
							{
								score++;
							}
						}

						// if this block row has the same score as the best block row, there is a 50% chance it will be chosen instead
						if( score == row1score && rnd.nextInt(2) == 0 )
						{
							row1score = score;
							p1rowblock1 = i;
							// choose a random row in this block
							p1row1 = i * 2 + rnd.nextInt(2);
						}
						// if this block row's score is better than the old best score, choose ths block row
						if( score > row1score )
						{
							row1score = score;
							p1rowblock1 = i;
							// choose a random row in this block
							p1row1 = i * 2 + rnd.nextInt(2);
						}
					}
					// now choose the best row, or a random row if there was no best row
					if( p1row1 == 0 )
						p1destr = rnd.nextInt(N);
					else
						p1destr = p1row1;

					p1col1 = 0;
					p1colblock1 = 0;
					col1score = 0;
					// check each column of blocks
					for( int i = 0; i < N/2; i++ )
					{
						score = 0;
						// check each block in that column
						for( int j = 0; j < N/2; j++ )
						{
							// score one point for each block owned by the other player
							if( B[j * 2][i * 2] == 2 )
							{
								score++;
							}
						}

						// if this block column has the same score as the best block column, there is a 50% chance it will be chosen instead
						if( score == col1score && rnd.nextInt(2) == 0 )
						{
							col1score = score;
							p1colblock1 = i;
							// choose a random column in this block
							p1col1 = i * 2 + rnd.nextInt(2);
						}
						// if this block column's score is better than the old best score, choose ths block column
						if( score > col1score )
						{
							col1score = score;
							p1colblock1 = i;
							// choose a random column in this block
							p1col1 = i * 2 + rnd.nextInt(2);
						}
					}
					// now choose the best column, or a random column if there was no best column
					if( p1col1 == 0 )
						p1destc = rnd.nextInt(N);
					else
						p1destc = p1col1;

					// store the location of the other row and column in the block
					p1row1 = p1destr;
					p1col1 = p1destc;
					if( (p1row1 % 2) == 0 )
						otherp1row1 = p1row1 + 1;
					if( (p1row1 % 2) == 1 )
						otherp1row1 = p1row1 - 1;
					if( (p1col1 % 2) == 0 )
						otherp1col1 = p1col1 + 1;
					if( (p1col1 % 2) == 1 )
						otherp1col1 = p1col1 - 1;

					p1row2 = 0;
					p1rowblock2 = 0;
					row2score = 0;
					// check each row
					for( int i = 0; i < N/2; i++ )
					{
						score = 0;
						// check each block in the row
						for( int j = 0; j < N/2; j++ )
						{
							// score one point for each block owned by the other player
							if( B[i * 2][j * 2] == 2 )
							{
								score++;
							}
						}

						// if this block row has the same score as the best block row, there is a 50% chance it will be chosen instead
						if( score == row2score && rnd.nextInt(2) == 0 )
						{
							temp = i * 2 + rnd.nextInt(2);
							// choose the same block as last time only if the row's score is less than half the maximum score
							//	and the row's score is less than the column's score
							// I think this is backwards, but it doesn't seem to make much diffeence.
							if( (temp != p1row1 && temp != otherp1row1) || (row1score < N / 4 && row1score < col1score) )
							{
								p1rowblock2 = i;
								row2score = score;
								p1row2 = temp;
							}
						}

						// if this block row's score is better than the old best score, choose ths block row
						if( score > row2score )
						{
							temp = i * 2 + rnd.nextInt(2);
							// choose the same block as last time only if the row's score is less than half the maximum score
							//	and the row's score is less than the column's score
							// I think this is backwards, but it doesn't seem to make much diffeence.
							if( (temp != p1row1 && temp != otherp1row1) || (row1score < N / 4 && row1score < col1score) )
							{
								p1rowblock2 = i;
								row2score = score;
								p1row2 = temp;
							}
						}
					}
					// now choose the best row, or a random row if there was no best row
					if( p1row2 == 0 )
						p1destr = rnd.nextInt(N);
					else
						p1destr = p1row2;

					p1col2 = 0;
					p1colblock2 = 0;
					col2score = 0;
					// check each column of blocks
					for( int i = 0; i < N/2; i++ )
					{
						score = 0;
						// check each block in that column
						for( int j = 0; j < N/2; j++ )
						{
							// score one point for each block owned by the other player
							if( B[j * 2][i * 2] == 2 )
							{
								score++;
							}
						}

						// if this block column has the same score as the best block column, there is a 50% chance it will be chosen instead
						if( score == col2score && rnd.nextInt(2) == 0 )
						{
							temp = i * 2 + rnd.nextInt(2);
							// choose the same block as last time only if the row's score is less than half the maximum score
							//	and the row's score is less than the column's score
							// I think this is backwards, but it doesn't seem to make much diffeence.
							if( (temp != p1col1 && temp != otherp1col1) || (col1score < N / 4 && col1score < row1score) )
							{
								p1colblock2 = i;
								col2score = score;
								p1col2 = temp;
							}
						}
						// if this block column's score is better than the old best score, choose ths block column
						if( score > col2score )
						{
							temp = i * 2 + rnd.nextInt(2);
							// choose the same block as last time only if the row's score is less than half the maximum score
							//	and the row's score is less than the column's score
							// I think this is backwards, but it doesn't seem to make much diffeence.
							if( (temp != p1col1 && temp != otherp1col1) || (col1score < N / 4 && col1score < row1score) )
							{
								p1colblock2 = i;
								col2score = score;
								p1col2 = temp;
							}
						}
					}
					// now choose the best column, or a random column if there was no best column
					if( p1col2 == 0 )
						p1destc = rnd.nextInt(N);
					else
						p1destc = p1col2;

					p1row2 = p1destr;
					p1col2 = p1destc;

					// if the strategy is 4, check which entries in the blocks will result in the best score
					if( p1strategy == 4 )
					{
						maxtp1p = 0;
						maxg = 1;
						maxh = 1;
						for( g = 1; g <= 4; g++ )
						{
							if( g == 1 )
							{
								p1row1 = p1rowblock1 * 2 + 1;
								p1col1 = p1colblock1 * 2 + 1;
							}
							if( g == 2 )
							{
								p1row1 = p1rowblock1 * 2 + 1;
								p1col1 = p1colblock1 * 2;
							}
							if( g == 3 )
							{
								p1row1 = p1rowblock1 * 2;
								p1col1 = p1colblock1 * 2 + 1;
							}
							if( g == 4 )
							{
								p1row1 = p1rowblock1 * 2;
								p1col1 = p1colblock1 * 2;
							}
							for( h = 1; h < 4; h++ )
							{
								if( h == 1 )
								{
									p1row2 = p1rowblock2 * 2 + 1;
									p1col2 = p1colblock2 * 2 + 1;
								}
								if( h == 2 )
								{
									p1row2 = p1rowblock2 * 2 + 1;
									p1col2 = p1colblock2 * 2;
								}
								if( h == 3 )
								{
									p1row2 = p1rowblock2 * 2;
									p1col2 = p1colblock2 * 2 + 1;
								}
								if( h == 4 )
								{
									p1row2 = p1rowblock2 * 2;
									p1col2 = p1colblock2 * 2;
								}

								for( int i = 0; i < N; i++ )
								{
									temp = A[p1row1][i];
									A[p1row1][i] = A[p1row2][i];
									A[p1row2][i] = temp;
								}
								for( int i = 0; i < N; i++ )
								{
									temp = A[i][p1col1];
									A[i][p1col1] = A[i][p1col2];
									A[i][p1col2] = temp;
								}

								tp1p = 0;
								tp2p = 0;
								for( int i = 0; i < N/2; i++ )
								{
									for( int j = 0; j < N/2; j++ )
									{
										sum = A[2 * i + 1][2 * j + 1]
												+ A[2 * i + 1][2 * j]
														+ A[2 * i]	[2 * j + 1]
																+ A[2 * i]	[2 * j];
										if( sum == 2 || sum == -2 )
											tp2p++;
										else
											tp1p++;
									}
								}

								for( int i = 0; i < N; i++ )
								{
									temp = A[p1row1][i];
									A[p1row1][i] = A[p1row2][i];
									A[p1row2][i] = temp;
								}
								for( int i = 0; i < N; i++ )
								{
									temp = A[i][p1col1];
									A[i][p1col1] = A[i][p1col2];
									A[i][p1col2] = temp;
								}

								if( tp1p == maxtp1p && rnd.nextInt(2) == 0 )
								{
									maxtp1p = tp1p;
									maxg = g;
									maxh = h;
								}

								if( tp1p > maxtp1p )
								{
									maxtp1p = tp1p;
									maxg = g;
									maxh = h;
								}
							}
						}

						if( maxg == 1 ){
							p1row1 = p1rowblock1 * 2 + 1;
							p1col1 = p1colblock1 * 2 + 1;
						}
						if( maxg == 2 ){
							p1row1 = p1rowblock1 * 2 + 1;
							p1col1 = p1colblock1 * 2;
						}
						if( maxg == 3 ){
							p1row1 = p1rowblock1 * 2;
							p1col1 = p1colblock1 * 2 + 1;
						}
						if( maxg == 4 ){
							p1row1 = p1rowblock1 * 2;
							p1col1 = p1colblock1 * 2;
						}

						if( maxh == 1 ){
							p1row2 = p1rowblock2 * 2 + 1;
							p1col2 = p1colblock2 * 2 + 1;
						}
						if( maxh == 2 ){
							p1row2 = p1rowblock2 * 2 + 1;
							p1col2 = p1colblock2 * 2;
						}
						if( maxh == 3 ){
							p1row2 = p1rowblock2 * 2;
							p1col2 = p1colblock2 * 2 + 1;
						}
						if( maxh == 4 ){
							p1row2 = p1rowblock2 * 2;
							p1col2 = p1colblock2 * 2;
						}
					}
					p1ready = 1;
				}
				if( p1ready == 1 && p1m == 0 && p1go == 0 ){
					p1destr = p1row1;
					p1destc = p1col1;
					p1go = 1;
				}
				if( p1ready == 1 && p1m == 1 && p1go == 0 ){
					p1destr = p1row2;
					p1destc = p1col2;
					p1ready = 0;
					p1go = 1;
				}
			}
			
			if( p1strategy == 5 ){
				if( rnd.nextInt(p1think) == 0 && p1ready == 0 && p1go == 0 ){
					int j, k, y, z;
					int temp1, temp2;


					// now start the main loop, it's finished if all the blocks are blue
					// or if every combination has been tested

					// was a while loop
					if( level >= 0 && level < N/2 && !done )
					{
						if( !sortingrows )
						{
							// first decide which positive column to put in column 1
							if( level == 0 )
							{
								if( !firstiteration )
									poscolused[firstchoice] = 0;
								firstiteration = false;
								firstchoice++;
								while( poscolused[firstchoice] == 1 ) // this should be unnecessary
									firstchoice++;

								//colindex[1] = poscolindex[firstchoice];
								p1row1 = 0;
								p1col1 = 1;
								p1row2 = 0;
								p1col2 = colcurrpos[poscolindex[firstchoice]];
								p1ready = 1;
								poscolused[firstchoice] = 1;
							}
							// next decide which column to pair with the next available positive column
							else if( level > 0 && level <= numposlevels )
							{
								// first check if the left column needs to be updated
								//  if it does, then put the next available positive column there
								if( nextposcol[level] == -1 )
								{
									firstiteration = true;
									nextposcol[level]++;
									while( poscolused[nextposcol[level]] == 1 )
										nextposcol[level]++;

									//colindex[level*2] = poscolindex[nextposcol[level]];
									p1row1 = 0;
									p1col1 = level*2;
									p1row2 = 0;
									p1col2 = colcurrpos[poscolindex[nextposcol[level]]];
									p1ready = 1;
									leftposcol[level] = nextposcol[level];
									poscolused[nextposcol[level]] = 1;

									previousswap = true;
								}
								if( !previousswap )
								{
									// now put the positive column whose turn is next in the right column.
									// we can assume that there is a next column available, since
									//	there is code later that checks if there are none left
									// if this is the first iteration, then don't remove the previous column
									if( !firstiteration )
										poscolused[nextposcol[level]] = 0;
									firstiteration = false;
									nextposcol[level]++;
									while( poscolused[nextposcol[level]] == 1 )
										nextposcol[level]++;
									//colindex[level*2+1] = poscolindex[nextposcol[level]];
									p1row1 = 0;
									p1col1 = level*2+1;
									p1row2 = 0;
									p1col2 = colcurrpos[poscolindex[nextposcol[level]]];
									p1ready = 1;

									poscolused[nextposcol[level]] = 1;
								}
							}
							// next decide which column to pair with the next available negative column
							else if( level > numposlevels && level <= numposlevels + numneglevels )
							{
								// first check if the left column needs to be updated
								//  if it does, then put the next available negaitive column there
								if( nextnegcol[level] == -1 )
								{
									firstiteration = true;
									nextnegcol[level]++;
									while( negcolused[nextnegcol[level]] == 1 )
										nextnegcol[level]++;
									//colindex[level*2] = negcolindex[nextnegcol[level]];
									p1row1 = 0;
									p1col1 = level*2;
									p1row2 = 0;
									p1col2 = colcurrpos[negcolindex[nextnegcol[level]]];
									p1ready = 1;
									leftnegcol[level] = nextnegcol[level];
									negcolused[nextnegcol[level]] = 1;

									previousswap = true;
								}
								if( !previousswap )
								{
									// now put the negative column whose turn is next in the right column
									// we can assume that there is a next column available, since
									//	there is code later that checks if there are none left
									// if this is the first iteration, then don't remove the previous column
									if( !firstiteration )
										negcolused[nextnegcol[level]] = 0;
									firstiteration = false;
									nextnegcol[level]++;
									while( negcolused[nextnegcol[level]] == 1 )
										nextnegcol[level]++;
									//colindex[level*2+1] = negcolindex[nextnegcol[level]];
									p1row1 = 0;
									p1col1 = level*2+1;
									p1row2 = 0;
									p1col2 = colcurrpos[negcolindex[nextnegcol[level]]];
									p1ready = 1;

									negcolused[nextnegcol[level]] = 1;
								}
							}

							if( !previousswap )
							{
								// now test if this combination is still acceptable
								goback = false;
								groupstart = 2;
								groupnum = 0;
								ourgroupnum = 0;
								// use the recursive sorting method to place the rows in the right order.
								// since we are only doing one level at a time, we don't need recursion,
								//  but we need to manually keep track of where the groups start and end.
								// this information is stored in the array sizes.

								sortingrows = true;
							}
							else
								previousswap = false;
						}
						else
						{
							// iterate through each group
							// was a while loop
							if( groupstart < N && !goback )
							{
								if( !rowsortloop2 )
								{
									// first find where the current group starts and ends.
									// on the first level, the group consists of rows 2 through n
									if( level == 0 )
									{
										groupsize = N-2;
										groupend = N-1;
									}
									// on the other levels, read the sizes of the groups from the array sizes
									else
									{
										groupsize = sizes[level-1][groupnum];
										groupend = groupstart + groupsize - 1;
										groupnum++;
									}
									// now sort the rows in this group, and identify the type of each pair at this level
									savedi = groupstart;
									numdiffs = 0;

									rowsortloop2 = true;
								}
								// was a while loop
								else if( savedi <= groupend - numdiffs )
								{
									int temp = 0;
									// store the type number in the right column's type entry,
									//	but not the left column's
									//		  type[savedi][level*2+1] =
									//			abs(   A[rowindex[savedi]][colindex[level*2	 ]]
									//				 + A[rowindex[savedi]][colindex[level*2+1]] );
									// if the entries are the same, move to the next row
									//	otherwise, switch this row with the last row in this group
									//	and check this row again
									//				  if( Math.abs(	  A[rowindex[savedi]][colindex[level*2	]]
									//								+ A[rowindex[savedi]][colindex[level*2+1]] ) == 0 )
									if( Math.abs(	A[savedi][level*2  ]
											+ A[savedi][level*2+1] ) == 0 )
									{
										//temp = rowindex[savedi];
										//rowindex[savedi] = rowindex[groupend - numdiffs];
										//rowindex[groupend - numdiffs] = temp;
										p1row1 = rowcurrpos[rowindex[savedi]];
										p1col1 = level*2+1;
										p1row2 = rowcurrpos[rowindex[groupend - numdiffs]];
										p1col2 = level*2+1;
										p1ready = 1;
										numdiffs++;
									}
									else
										savedi++;
								}
								else
								{
									rowsortloop2 = false;
								}

								if( !rowsortloop2 )
								{
									// if there are an odd number of differents, then the break between
									//	sames and differents is in the middle of a block,
									//	so that block is still green and we need to back up a level.
									if( numdiffs % 2 != 0 )
									{
										goback = true;
									}
									// otherwise, add the two new groups to this level's list of groups
									else
									{
										if( groupsize - numdiffs > 0 )
										{
											sizes[level][ourgroupnum] = groupsize - numdiffs;
											ourgroupnum++;
										}
										if( numdiffs > 0 )
										{
											sizes[level][ourgroupnum] = numdiffs;
											ourgroupnum++;
										}
										// standardize the groups to the form ++, --, +-, -+
									}
									// now update the group start position and restart the loop to check
									//	where the next group ends
									groupstart = groupend + 1;
								}
							}
							else
							{
								// move to the next level only if this level is all blue
								if( !goback )
									level++;
								else
								{
									finishedpopping = false;
									while( !finishedpopping )
									{
										// check if the next iteration is valid
										nextfirstchoice = firstchoice;
										if( nextfirstchoice >= 0 )
											while( nextfirstchoice < numposcols && poscolused[nextfirstchoice] != 0 )
												nextfirstchoice++;
										nextnextposcol = nextposcol[level];
										if( nextnextposcol >= 0 )
											while( nextnextposcol < numposcols && poscolused[nextnextposcol] != 0 )
												nextnextposcol++;
										nextnextnegcol = nextnegcol[level];
										if( nextnextnegcol >= 0 )
											while( nextnextnegcol < numnegcols && negcolused[nextnextnegcol] != 0 )
												nextnextnegcol++;
										// the matrix is unsolveable if the first level has been exhausted
										if( level == 0 && nextfirstchoice >= numposcols )
										{
											done = true;
											success = false;
											finishedpopping = true;
										}
										// move down a level if the current positive level is exhausted
										else if( level > 0 && level <= numposlevels &&  nextnextposcol >= numposcols )
										{
											// free the active two columns
											poscolused[leftposcol[level]] = 0;
											poscolused[nextposcol[level]] = 0;
											leftposcol[level] = -1;
											nextposcol[level] = -1;
											level--;
										}
										// move down a level if the current negative level is exhausted
										else if( level > numposlevels && level <= numposlevels + numneglevels
												&& nextnextnegcol >= numnegcols )
										{
											// free the active two columns
											negcolused[leftnegcol[level]] = 0;
											negcolused[nextnegcol[level]] = 0;
											leftnegcol[level] = -1;
											nextnegcol[level] = -1;
											level--;
										}
										else
											finishedpopping = true;
									}
								}
								sortingrows = false;
							}
						}
					}
					else
					{
						if( level >= N/2 )
							success = true;

						if( success )
						{
							//if( !answeronly )
							System.out.println( "Success!\n\n" );
						}
						else
							System.out.println( "Failure!\n\n" );
					}
				}

				if( p1ready == 1 && p1m == 0 && p1go == 0 )
				{
					p1destr = p1row1;
					p1destc = p1col1;
					p1go = 1;
				}
				if( p1ready == 1 && p1m == 1 && p1go == 0 )
				{
					p1destr = p1row2;
					p1destc = p1col2;
					p1ready = 0;
					p1go = 1;
				}
			}

			if( p1strategy == 6 )
			{
				int i, j, k, y, z;
				int temp, temp1, temp2;

				// the main loop, it's finished if all the blocks are blue
				// or if every combination has been tested

				// this was a while loop
				if( level >= 0 && level < N/2 && !done )
				{
					// first decide which positive column to put in column 1
					if( level == 0 )
					{
						if( !firstiteration )
							poscolused[firstchoice] = 0;
						firstiteration = false;
						firstchoice++;
						while( poscolused[firstchoice] == 1 ) // this should be unnecessary
							firstchoice++;
						//colindex[1] = poscolindex[firstchoice];
						swap( 0, 1, 0, colcurrpos[poscolindex[firstchoice]] );
						poscolused[firstchoice] = 1;
					}
					// next decide which column to pair with the next available positive column
					else if( level > 0 && level <= numposlevels )
					{
						// first check if the left column needs to be updated
						//  if it does, then put the next available positive column there
						if( nextposcol[level] == -1 )
						{
							firstiteration = true;
							nextposcol[level]++;
							while( poscolused[nextposcol[level]] == 1 )
								nextposcol[level]++;
							//colindex[level*2] = poscolindex[nextposcol[level]];
							swap( 0, level*2, 0, colcurrpos[poscolindex[nextposcol[level]]] );
							leftposcol[level] = nextposcol[level];
							poscolused[nextposcol[level]] = 1;
						}
						// now put the positive column whose turn is next in the right column.
						// we can assume that there is a next column available, since
						//  there is code later that checks if there are none left
						// if this is the first iteration, then don't remove the previous column
						if( !firstiteration )
							poscolused[nextposcol[level]] = 0;
						firstiteration = false;
						nextposcol[level]++;
						while( poscolused[nextposcol[level]] == 1 )
							nextposcol[level]++;
						//colindex[level*2+1] = poscolindex[nextposcol[level]];
						swap( 0, level*2+1, 0, colcurrpos[poscolindex[nextposcol[level]]] );
						poscolused[nextposcol[level]] = 1;
					}
					// next decide which column to pair with the next available negative column
					else if( level > numposlevels && level <= numposlevels + numneglevels )
					{
						// first check if the left column needs to be updated
						//  if it does, then put the next available negaitive column there
						if( nextnegcol[level] == -1 )
						{
							firstiteration = true;
							nextnegcol[level]++;
							while( negcolused[nextnegcol[level]] == 1 )
								nextnegcol[level]++;
							//colindex[level*2] = negcolindex[nextnegcol[level]];
							swap( 0, level*2, 0, colcurrpos[negcolindex[nextnegcol[level]]] );
							leftnegcol[level] = nextnegcol[level];
							negcolused[nextnegcol[level]] = 1;
						}
						// now put the negative column whose turn is next in the right column
						// we can assume that there is a next column available, since
						//  there is code later that checks if there are none left
						// if this is the first iteration, then don't remove the previous column
						if( !firstiteration )
							negcolused[nextnegcol[level]] = 0;
						firstiteration = false;
						nextnegcol[level]++;
						while( negcolused[nextnegcol[level]] == 1 )
							nextnegcol[level]++;
						//colindex[level*2+1] = negcolindex[nextnegcol[level]];
						swap( 0, level*2+1, 0, colcurrpos[negcolindex[nextnegcol[level]]] );
						negcolused[nextnegcol[level]] = 1;
					}

					// now test if this combination is still acceptable
					goback = false;
					groupstart = 2;
					groupnum = 0;
					ourgroupnum = 0;
					// use the recursive sorting method to place the rows in the right order.
					// since we are only doing one level at a time, we don't need recursion,
					//	but we need to manually keep track of where the groups start and end.
					// this information is stored in the array sizes.
					while( groupstart < N && !goback )
					{
						// first find where the current group starts and ends.
						// on the first level, the group consists of rows 2 through n
						if( level == 0 )
						{
							groupsize = N-2;
							groupend = N-1;
						}
						// on the other levels, read the sizes of the groups from the array sizes
						else
						{
							groupsize = sizes[level-1][groupnum];
							groupend = groupstart + groupsize - 1;
							groupnum++;
						}
						// now sort the rows in this group, and identify the type of each pair at this level
						i = groupstart;
						numdiffs = 0;
						while( i <= groupend - numdiffs )
						{
							// store the type number in the right column's type entry,
							//	but not the left column's
							//		  type[i][level*2+1] =
							//			abs(   A[rowindex[i]][colindex[level*2	]]
							//				 + A[rowindex[i]][colindex[level*2+1]] );
							// if the entries are the same, move to the next row
							//	otherwise, switch this row with the last row in this group
							//	and check this row again
							if( Math.abs(	A[i][level*2  ]
									+ A[i][level*2+1] ) == 0 )
							{
								//temp = rowindex[i];
								//rowindex[i] = rowindex[groupend - numdiffs];
								//rowindex[groupend - numdiffs] = temp;
								swap( rowcurrpos[rowindex[i]], 0, rowcurrpos[rowindex[groupend - numdiffs]], 0 );
								numdiffs++;
							}
							else
								i++;
						}
						// if there are an odd number of differents, then the break between
						//  sames and differents is in the middle of a block,
						//  so that block is still green and we need to back up a level.
						if( numdiffs % 2 != 0 )
						{
							goback = true;
						}
						// otherwise, add the two new groups to this level's list of groups
						else{
							if( groupsize - numdiffs > 0 )
							{
								sizes[level][ourgroupnum] = groupsize - numdiffs;
								ourgroupnum++;
							}
							if( numdiffs > 0 )
							{
								sizes[level][ourgroupnum] = numdiffs;
								ourgroupnum++;
							}
							// standardize the groups to the form ++, --, +-, -+
							if( standardize )
							{
								i = groupstart;
								numdiffs2 = 0;
								while( i <= groupend - numdiffs - numdiffs2 )
								{
									// if the type is ++, move to the next row
									//	otherwise, switch this row with the last row in this subgroup
									//	and check this row again
									if(	   A[i][level*2	 ] == -1
											&& A[i][level*2+1] == -1 )
									{
										//temp = rowindex[i];
										//rowindex[i] = rowindex[groupend - numdiffs - numdiffs2];
										//rowindex[groupend - numdiffs - numdiffs2] = temp;
										swap( rowcurrpos[rowindex[i]], 0, rowcurrpos[rowindex[groupend - numdiffs - numdiffs2]], 0 );
										numdiffs2++;
									}
									else
										i++;
								}
								i = groupend - numdiffs + 1;
								numdiffs2 = 0;
								while( i <= groupend - numdiffs2 )
								{
									// if the type is +-, move to the next row
									//	otherwise, switch this row with the last row in this subgroup
									//	and check this row again
									if(	   A[i][level*2	 ] == -1
											&& A[i][level*2+1] ==  1 )
									{
										//temp = rowindex[i];
										//rowindex[i] = rowindex[groupend - numdiffs2];
										//rowindex[groupend - numdiffs2] = temp;
										swap( rowcurrpos[rowindex[i]], 0, rowcurrpos[rowindex[groupend - numdiffs2]], 0 );
										numdiffs2++;
									}
									else
										i++;
								}
							}
						}
						// now update the group start position and restart the loop to check
						//  where the next group ends
						groupstart = groupend + 1;
					}

					// move to the next level only if this level is all blue
					if( !goback )
						level++;
					else
					{
						finishedpopping = false;
						while( !finishedpopping )
						{
							// check if the next iteration is valid
							nextfirstchoice = firstchoice;
							if( nextfirstchoice >= 0 )
								while( nextfirstchoice < numposcols && poscolused[nextfirstchoice] != 0 )
									nextfirstchoice++;
							nextnextposcol = nextposcol[level];
							if( nextnextposcol >= 0 )
								while( nextnextposcol < numposcols && poscolused[nextnextposcol] != 0 )
									nextnextposcol++;
							nextnextnegcol = nextnegcol[level];
							if( nextnextnegcol >= 0 )
								while( nextnextnegcol < numnegcols && negcolused[nextnextnegcol] != 0 )
									nextnextnegcol++;
							// the matrix is unsolveable if the first level has been exhausted
							if( level == 0 && nextfirstchoice >= numposcols )
							{
								done = true;
								success = false;
								finishedpopping = true;
							}
							// move down a level if the current positive level is exhausted
							else if( level > 0 && level <= numposlevels &&	nextnextposcol >= numposcols )
							{
								// free the active two columns
								poscolused[leftposcol[level]] = 0;
								poscolused[nextposcol[level]] = 0;
								leftposcol[level] = -1;
								nextposcol[level] = -1;
								level--;
							}
							// move down a level if the current negative level is exhausted
							else if( level > numposlevels && level <= numposlevels + numneglevels
									&& nextnextnegcol >= numnegcols )
							{
								// free the active two columns
								negcolused[leftnegcol[level]] = 0;
								negcolused[nextnegcol[level]] = 0;
								leftnegcol[level] = -1;
								nextnegcol[level] = -1;
								level--;
							}
							else
								finishedpopping = true;
						}
					}
				}else{
					if( level >= N/2 )
						success = true;

					if( success ){
						//if( !answeronly )
						System.out.println( "Success!\n\n" );
						p1a = 0;
						p1strategy = 0;
					}
					else
						System.out.println( "Failure!\n\n" );
				}
				refresh = 1;
			}

			if( p1strategy == 7 )
			{
				int i, j, k, y, z;
				int temp, temp1, temp2;

				// the main loop, it's finished if all the blocks are blue
				// or if every combination has been tested

				// this was a while loop
				while( level >= 0 && level < N/2 && !done )
				{
					// first decide which positive column to put in column 1
					if( level == 0 )
					{
						if( !firstiteration )
							poscolused[firstchoice] = 0;
						firstiteration = false;
						firstchoice++;
						while( poscolused[firstchoice] == 1 ) // this should be unnecessary
							firstchoice++;
						colindex[1] = poscolindex[firstchoice];
						poscolused[firstchoice] = 1;
					}
					// next decide which column to pair with the next available positive column
					else if( level > 0 && level <= numposlevels )
					{
						// first check if the left column needs to be updated
						//  if it does, then put the next available positive column there
						if( nextposcol[level] == -1 )
						{
							firstiteration = true;
							nextposcol[level]++;
							while( poscolused[nextposcol[level]] == 1 )
								nextposcol[level]++;
							colindex[level*2] = poscolindex[nextposcol[level]];
							leftposcol[level] = nextposcol[level];
							poscolused[nextposcol[level]] = 1;
						}
						// now put the positive column whose turn is next in the right column.
						// we can assume that there is a next column available, since
						//  there is code later that checks if there are none left
						// if this is the first iteration, then don't remove the previous column
						if( !firstiteration )
							poscolused[nextposcol[level]] = 0;
						firstiteration = false;
						nextposcol[level]++;
						while( poscolused[nextposcol[level]] == 1 )
							nextposcol[level]++;
						colindex[level*2+1] = poscolindex[nextposcol[level]];
						poscolused[nextposcol[level]] = 1;
					}
					// next decide which column to pair with the next available negative column
					else if( level > numposlevels && level <= numposlevels + numneglevels )
					{
						// first check if the left column needs to be updated
						//  if it does, then put the next available negaitive column there
						if( nextnegcol[level] == -1 )
						{
							firstiteration = true;
							nextnegcol[level]++;
							while( negcolused[nextnegcol[level]] == 1 )
								nextnegcol[level]++;
							colindex[level*2] = negcolindex[nextnegcol[level]];
							leftnegcol[level] = nextnegcol[level];
							negcolused[nextnegcol[level]] = 1;
						}
						// now put the negative column whose turn is next in the right column
						// we can assume that there is a next column available, since
						//  there is code later that checks if there are none left
						// if this is the first iteration, then don't remove the previous column
						if( !firstiteration )
							negcolused[nextnegcol[level]] = 0;
						firstiteration = false;
						nextnegcol[level]++;
						while( negcolused[nextnegcol[level]] == 1 )
							nextnegcol[level]++;
						colindex[level*2+1] = negcolindex[nextnegcol[level]];
						negcolused[nextnegcol[level]] = 1;
					}

					// now test if this combination is still acceptable
					goback = false;
					groupstart = 2;
					groupnum = 0;
					ourgroupnum = 0;
					// use the recursive sorting method to place the rows in the right order.
					// since we are only doing one level at a time, we don't need recursion,
					//	but we need to manually keep track of where the groups start and end.
					// this information is stored in the array sizes.
					while( groupstart < N && !goback )
					{
						// first find where the current group starts and ends.
						// on the first level, the group consists of rows 2 through n
						if( level == 0 )
						{
							groupsize = N-2;
							groupend = N-1;
						}
						// on the other levels, read the sizes of the groups from the array sizes
						else
						{
							groupsize = sizes[level-1][groupnum];
							groupend = groupstart + groupsize - 1;
							groupnum++;
						}
						// now sort the rows in this group, and identify the type of each pair at this level
						i = groupstart;
						numdiffs = 0;
						while( i <= groupend - numdiffs )
						{
							// store the type number in the right column's type entry,
							//	but not the left column's
							//		  type[i][level*2+1] =
							//			abs(   A[rowindex[i]][colindex[level*2	]]
							//				 + A[rowindex[i]][colindex[level*2+1]] );
							// if the entries are the same, move to the next row
							//	otherwise, switch this row with the last row in this group
							//	and check this row again
							if( Math.abs(	A[rowindex[i]][colindex[level*2	 ]]
									+ A[rowindex[i]][colindex[level*2+1]] ) == 0 )
							{
								temp = rowindex[i];
								rowindex[i] = rowindex[groupend - numdiffs];
								rowindex[groupend - numdiffs] = temp;
								numdiffs++;
							}
							else
								i++;
						}
						// if there are an odd number of differents, then the break between
						//  sames and differents is in the middle of a block,
						//  so that block is still green and we need to back up a level.
						if( numdiffs % 2 != 0 )
						{
							goback = true;
						}
						// otherwise, add the two new groups to this level's list of groups
						else
						{
							if( groupsize - numdiffs > 0 )
							{
								sizes[level][ourgroupnum] = groupsize - numdiffs;
								ourgroupnum++;
							}
							if( numdiffs > 0 )
							{
								sizes[level][ourgroupnum] = numdiffs;
								ourgroupnum++;
							}
							// standardize the groups to the form ++, --, +-, -+
							if( standardize )
							{
								i = groupstart;
								numdiffs2 = 0;
								while( i <= groupend - numdiffs - numdiffs2 )
								{
									// if the type is ++, move to the next row
									//	otherwise, switch this row with the last row in this subgroup
									//	and check this row again
									if(	   A[rowindex[i]][colindex[level*2	]] == -1
											&& A[rowindex[i]][colindex[level*2+1]] == -1 )
									{
										temp = rowindex[i];
										rowindex[i] = rowindex[groupend - numdiffs - numdiffs2];
										rowindex[groupend - numdiffs - numdiffs2] = temp;
										numdiffs2++;
									}
									else
										i++;
								}
								i = groupend - numdiffs + 1;
								numdiffs2 = 0;
								while( i <= groupend - numdiffs2 )
								{
									// if the type is +-, move to the next row
									//	otherwise, switch this row with the last row in this subgroup
									//	and check this row again
									if(	   A[rowindex[i]][colindex[level*2	]] == -1
											&& A[rowindex[i]][colindex[level*2+1]] ==  1 )
									{
										temp = rowindex[i];
										rowindex[i] = rowindex[groupend - numdiffs2];
										rowindex[groupend - numdiffs2] = temp;
										numdiffs2++;
									}
									else
										i++;
								}
							}
						}
						// now update the group start position and restart the loop to check
						//  where the next group ends
						groupstart = groupend + 1;
					}

					// move to the next level only if this level is all blue
					if( !goback )
						level++;
					else
					{
						finishedpopping = false;
						while( !finishedpopping )
						{
							// check if the next iteration is valid
							nextfirstchoice = firstchoice;
							if( nextfirstchoice >= 0 )
								while( nextfirstchoice < numposcols && poscolused[nextfirstchoice] != 0 )
									nextfirstchoice++;
							nextnextposcol = nextposcol[level];
							if( nextnextposcol >= 0 )
								while( nextnextposcol < numposcols && poscolused[nextnextposcol] != 0 )
									nextnextposcol++;
							nextnextnegcol = nextnegcol[level];
							if( nextnextnegcol >= 0 )
								while( nextnextnegcol < numnegcols && negcolused[nextnextnegcol] != 0 )
									nextnextnegcol++;
							// the matrix is unsolveable if the first level has been exhausted
							if( level == 0 && nextfirstchoice >= numposcols )
							{
								done = true;
								success = false;
								finishedpopping = true;
							}
							// move down a level if the current positive level is exhausted
							else if( level > 0 && level <= numposlevels &&	nextnextposcol >= numposcols )
							{
								// free the active two columns
								poscolused[leftposcol[level]] = 0;
								poscolused[nextposcol[level]] = 0;
								leftposcol[level] = -1;
								nextposcol[level] = -1;
								level--;
							}
							// move down a level if the current negative level is exhausted
							else if( level > numposlevels && level <= numposlevels + numneglevels
									&& nextnextnegcol >= numnegcols )
							{
								// free the active two columns
								negcolused[leftnegcol[level]] = 0;
								negcolused[nextnegcol[level]] = 0;
								leftnegcol[level] = -1;
								nextnegcol[level] = -1;
								level--;
							}
							else
								finishedpopping = true;
						}
					}
				}

				{
					if( level >= N/2 )
						success = true;

					if( success )
					{
						int r1 = 0;
						int c1 = 0;
						int r2 = 0;
						int c2 = 0;

						int[] rightrow = new int[N];
						int[] rightcol = new int[N];
						for( int h = 0; h < N; h++ )
						{
							rightrow[h] = rowindex[h];
							rightcol[h] = colindex[h];
							rowindex[h] = h;
							colindex[h] = h;
						}
						//			int temp;
						//if( !answeronly )
						System.out.println( "Success!\n\n" );

						// now put the rows and columns in the correct order
						for( int h = 0; h < N; h++ )
						{
							//swap( r, c, rowindex[r], colindex[c] );
							r1 = h;
							c1 = h;
							r2 = rowcurrpos[rightrow[h]];
							c2 = colcurrpos[rightcol[h]];
							temp = 0;

							for( int i2 = 0; i2 < N; i2++ )
							{
								temp = A[r1][i2];
								A[r1][i2] = A[r2][i2];
								A[r2][i2] = temp;
							}
							for( int i2 = 0; i2 < N; i2++ )
							{
								temp = A[i2][c1];
								A[i2][c1] = A[i2][c2];
								A[i2][c2] = temp;
							}
							checkscore();

							rowcurrpos[rowindex[r1]] = r2;
							rowcurrpos[rowindex[r2]] = r1;
							colcurrpos[colindex[c1]] = c2;
							colcurrpos[colindex[c2]] = c1;

							temp = rowindex[r1];
							rowindex[r1] = rowindex[r2];
							rowindex[r2] = temp;
							temp = colindex[c1];
							colindex[c1] = colindex[c2];
							colindex[c2] = temp;
						}
					}
					else
						System.out.println( "Failure!\n\n" );
				}

				p1a = 0;
				p1strategy = 0;
				refresh = 1;
			}

			// this is the code that "presses the buttons" if player 1 is automated
			if( p1strategy >= 2 && p1a == 1 )
			{
				if( p1go == 1 && p1movecheat == 0 )
				{
					int p1r = 0;
					int p1c = 0;
					if( p1m == 0 )
					{
						p1r = p1r1;
						p1c = p1c1;
					}
					else
					{
						p1r = p1r2;
						p1c = p1c2;
					}
					if( p1r < p1destr && rnd.nextInt(p1move) == 0 )
						kk = true;
					if( p1r > p1destr && rnd.nextInt(p1move) == 0 )
						ki = true;
					if( p1c < p1destc && rnd.nextInt(p1move) == 0 )
						kl = true;
					if( p1c > p1destc && rnd.nextInt(p1move) == 0 )
						kj = true;
					if( p1r == p1destr && p1c == p1destc && rnd.nextInt(p1choose) == 0 )
					{
						kf = true;
						p1go = 0;
					}
				}
				if( p1go == 1 && p1movecheat == 1 )
				{
					if( p1m == 0 )
					{
						p1r1 = p1destr;
						p1c1 = p1destc;
					}
					else
					{
						p1r2 = p1destr;
						p1c2 = p1destc;
					}
					kf = true;
					p1go = 0;
				}
			}


			if( p1m == 0 ){
				if( kj ){
					kj = false;
					p1c1--;
					refresh = 1;
					p1cyclenum = 0;
				}
				if( kl ){
					kl = false;
					p1c1++;
					refresh = 1;
					p1cyclenum = 0;
				}
				if( ki ){
					ki = false;
					p1r1--;
					refresh = 1;
					p1cyclenum = 0;
				}
				if( kk ){
					kk = false;
					p1r1++;
					refresh = 1;
					p1cyclenum = 0;
				}
			}else{
				if( kj ){
					kj = false;
					p1c2--;
					refresh = 1;
					p1cyclenum = 0;
				}
				if( kl ){
					kl = false;
					p1c2++;
					refresh = 1;
					p1cyclenum = 0;
				}
				if( ki ){
					ki = false;
					p1r2--;
					refresh = 1;
					p1cyclenum = 0;
				}
				if( kk ){
					kk = false;
					p1r2++;
					refresh = 1;
					p1cyclenum = 0;
				}
			}

			// switch the current block to a different state
			// this cycles through all 4 possible states
			/*
	if( A$ = "G" || B$ = "G" )

	  p1cyclenum = p1cyclenum + 1
	  if( p1cyclenum > 3 ) p1cyclenum = 1

	  if( p1cyclenum = 1 )
		if( (p1r1 % 2) = 0 )
		  p1r1 = p1r1 - 1
		else
		  p1r1 = p1r1 + 1
		}
		if( (p1c1 % 2) = 0 )
		  p1c1 = p1c1 - 1
		else
		  p1c1 = p1c1 + 1
		}
	  }
	  if( p1cyclenum = 2 )
		if( (p1r1 % 2) = 0 )
		  p1r1 = p1r1 - 1
		else
		  p1r1 = p1r1 + 1
		}
		if( (p1c1 % 2) = 0 )
		  p1c1 = p1c1 - 1
		else
		  p1c1 = p1c1 + 1
		}
		if( (p1r2 % 2) = 0 )
		  p1r2 = p1r2 - 1
		else
		  p1r2 = p1r2 + 1
		}
		if( (p1c2 % 2) = 0 )
		  p1c2 = p1c2 - 1
		else
		  p1c2 = p1c2 + 1
		}
	  }
	  if( p1cyclenum = 3 )
		if( (p1r1 % 2) = 0 )
		  p1r1 = p1r1 - 1
		else
		  p1r1 = p1r1 + 1
		}
		if( (p1c1 % 2) = 0 )
		  p1c1 = p1c1 - 1
		else
		  p1c1 = p1c1 + 1
		}
	  }

	  p1m = 1
	  A$ = "F"
	}
			 */


			// strategy 1 just presses a random button
			if( p2strategy == 1 && p2a == 1 ){
				if( rnd.nextInt(p2move) == 0 ){
					k4 = false;
					k5 = false;
					k4 = false;
					k6 = false;
					k0 = false;
					kd = false;

					int t = rnd.nextInt(5);
					switch(t)
					{
					case 0:
						k8 = true;
						break;
					case 1:
						k5 = true;
						break;
					case 2:
						k4 = true;
						break;
					case 3:
						k6 = true;
						break;
					case 4:
						k0 = true;
						break;
					}
				}
			}

			// strategy 2 is to move to a random destination, then choose it
			if( p2strategy == 2 && p2a == 1 ){
				if( p2go == 0 ){
					p2destr = rnd.nextInt(N);
					p2destc = rnd.nextInt(N);
					p2go = 1;
				}
			}

			// strategy 3 and 4 are to find the two blocks whose rows and columns have
			//	the lowest score, then switch them
			// strategy 4 also checks which elements in the block would be best to switch

			if( ( p2strategy == 3 || p2strategy == 4 ) && p2a == 1 ){
				// temporary variables
				int score = 0;
				int otherp2row1 = 0;
				int otherp2col1 = 0;
				int temp = 0;
				int maxtp2p = 0;
				int maxg = 0;
				int maxh = 0;
				int g = 0;
				int h = 0;
				int tp2p = 0;
				int tp1p = 0;
				int sum = 0;

				if( rnd.nextInt(p2think) == 0 && p2ready == 0 && p2go == 0 ){
					p2row1 = 0;
					p2rowblock1 = 0;
					row1score = 0;
					// search each row of blocks
					for( int i = 0; i < N/2; i++ ){
						score = 0;
						// check each block in that row
						for( int j = 0; j < N/2; j++ ){
							// score one point for each block owned by the other player
							if( B[i * 2][j * 2] == 1 ){
								score++;
							}
						}

						// if this block row has the same score as the best block row, there is a 50% chance it will be chosen instead
						if( score == row1score && rnd.nextInt(2) == 0 ){
							row1score = score;
							p2rowblock1 = i;
							// choose a random row in this block
							p2row1 = i * 2 + rnd.nextInt(2);
						}
						// if this block row's score is better than the old best score, choose ths block row
						if( score > row1score ){
							row1score = score;
							p2rowblock1 = i;
							// choose a random row in this block
							p2row1 = i * 2 + rnd.nextInt(2);
						}
					}
					// now choose the best row, or a random row if there was no best row
					if( p2row1 == 0 )
						p2destr = rnd.nextInt(N);
					else
						p2destr = p2row1;

					p2col1 = 0;
					p2colblock1 = 0;
					col1score = 0;
					// check each column of blocks
					for( int i = 0; i < N/2; i++ ){
						score = 0;
						// check each block in that column
						for( int j = 0; j < N/2; j++ ){
							// score one point for each block owned by the other player
							if( B[j * 2][i * 2] == 1 ){
								score++;
							}
						}

						// if this block column has the same score as the best block column, there is a 50% chance it will be chosen instead
						if( score == col1score && rnd.nextInt(2) == 0 ){
							col1score = score;
							p2colblock1 = i;
							// choose a random column in this block
							p2col1 = i * 2 + rnd.nextInt(2);
						}
						// if this block column's score is better than the old best score, choose ths block column
						if( score > col1score ){
							col1score = score;
							p2colblock1 = i;
							// choose a random column in this block
							p2col1 = i * 2 + rnd.nextInt(2);
						}
					}
					// now choose the best column, or a random column if there was no best column
					if( p2col1 == 0 )
						p2destc = rnd.nextInt(N);
					else
						p2destc = p2col1;

					// store the location of the other row and column in the block
					p2row1 = p2destr;
					p2col1 = p2destc;
					if( (p2row1 % 2) == 0 )
						otherp2row1 = p2row1 + 1;
					if( (p2row1 % 2) == 1 )
						otherp2row1 = p2row1 - 1;
					if( (p2col1 % 2) == 0 )
						otherp2col1 = p2col1 + 1;
					if( (p2col1 % 2) == 1 )
						otherp2col1 = p2col1 - 1;

					p2row2 = 0;
					p2rowblock2 = 0;
					row2score = 0;
					// check each row
					for( int i = 0; i < N/2; i++ ){
						score = 0;
						// check each block in the row
						for( int j = 0; j < N/2; j++ ){
							// score one point for each block owned by the other player
							if( B[i * 2][j * 2] == 1 ){
								score++;
							}
						}

						// if this block row has the same score as the best block row, there is a 50% chance it will be chosen instead
						if( score == row2score && rnd.nextInt(2) == 0 ){
							temp = i * 2 + rnd.nextInt(2);
							// choose the same block as last time only if the row's score is less than half the maximum score
							//	and the row's score is less than the column's score
							// I think this is backwards, but it doesn't seem to make much diffeence.
							if( (temp != p2row1 && temp != otherp2row1) || (row1score < N / 4 && row1score < col1score) ){
								p2rowblock2 = i;
								row2score = score;
								p2row2 = temp;
							}
						}

						// if this block row's score is better than the old best score, choose ths block row
						if( score > row2score ){
							temp = i * 2 + rnd.nextInt(2);
							// choose the same block as last time only if the row's score is less than half the maximum score
							//	and the row's score is less than the column's score
							// I think this is backwards, but it doesn't seem to make much diffeence.
							if( (temp != p2row1 && temp != otherp2row1) || (row1score < N / 4 && row1score < col1score) ){
								p2rowblock2 = i;
								row2score = score;
								p2row2 = temp;
							}
						}
					}
					// now choose the best row, or a random row if there was no best row
					if( p2row2 == 0 )
						p2destr = rnd.nextInt(N);
					else
						p2destr = p2row2;

					p2col2 = 0;
					p2colblock2 = 0;
					col2score = 0;
					// check each column of blocks
					for( int i = 0; i < N/2; i++ )
					{
						score = 0;
						// check each block in that column
						for( int j = 0; j < N/2; j++ )
						{
							// score one point for each block owned by the other player
							if( B[j * 2][i * 2] == 1 )
							{
								score++;
							}
						}

						// if this block column has the same score as the best block column, there is a 50% chance it will be chosen instead
						if( score == col2score && rnd.nextInt(2) == 0 )
						{
							temp = i * 2 + rnd.nextInt(2);
							// choose the same block as last time only if the row's score is less than half the maximum score
							//	and the row's score is less than the column's score
							// I think this is backwards, but it doesn't seem to make much diffeence.
							if( (temp != p2col1 && temp != otherp2col1) || (col1score < N / 4 && col1score < row1score) )
							{
								p2colblock2 = i;
								col2score = score;
								p2col2 = temp;
							}
						}
						// if this block column's score is better than the old best score, choose ths block column
						if( score > col2score )
						{
							temp = i * 2 + rnd.nextInt(2);
							// choose the same block as last time only if the row's score is less than half the maximum score
							//	and the row's score is less than the column's score
							// I think this is backwards, but it doesn't seem to make much diffeence.
							if( (temp != p2col1 && temp != otherp2col1) || (col1score < N / 4 && col1score < row1score) )
							{
								p2colblock2 = i;
								col2score = score;
								p2col2 = temp;
							}
						}
					}
					// now choose the best column, or a random column if there was no best column
					if( p2col2 == 0 )
						p2destc = rnd.nextInt(N);
					else
						p2destc = p2col2;

					p2row2 = p2destr;
					p2col2 = p2destc;

					// if the strategy is 4, check which entries in the blocks will result in the best score
					if( p2strategy == 4 )
					{
						maxtp2p = 0;
						maxg = 1;
						maxh = 1;
						for( g = 1; g <= 4; g++ )
						{
							if( g == 1 )
							{
								p2row1 = p2rowblock1 * 2 + 1;
								p2col1 = p2colblock1 * 2 + 1;
							}
							if( g == 2 )
							{
								p2row1 = p2rowblock1 * 2 + 1;
								p2col1 = p2colblock1 * 2;
							}
							if( g == 3 )
							{
								p2row1 = p2rowblock1 * 2;
								p2col1 = p2colblock1 * 2 + 1;
							}
							if( g == 4 )
							{
								p2row1 = p2rowblock1 * 2;
								p2col1 = p2colblock1 * 2;
							}
							for( h = 1; h < 4; h++ )
							{
								if( h == 1 )
								{
									p2row2 = p2rowblock2 * 2 + 1;
									p2col2 = p2colblock2 * 2 + 1;
								}
								if( h == 2 )
								{
									p2row2 = p2rowblock2 * 2 + 1;
									p2col2 = p2colblock2 * 2;
								}
								if( h == 3 )
								{
									p2row2 = p2rowblock2 * 2;
									p2col2 = p2colblock2 * 2 + 1;
								}
								if( h == 4 )
								{
									p2row2 = p2rowblock2 * 2;
									p2col2 = p2colblock2 * 2;
								}

								for( int i = 0; i < N; i++ )
								{
									temp = A[p2row1][i];
									A[p2row1][i] = A[p2row2][i];
									A[p2row2][i] = temp;
								}
								for( int i = 0; i < N; i++ )
								{
									temp = A[i][p2col1];
									A[i][p2col1] = A[i][p2col2];
									A[i][p2col2] = temp;
								}

								tp2p = 0;
								tp1p = 0;
								for( int i = 0; i < N/2; i++ )
								{
									for( int j = 0; j < N/2; j++ )
									{
										sum = A[2 * i + 1][2 * j + 1]
												+ A[2 * i + 1][2 * j]
														+ A[2 * i]	[2 * j + 1]
																+ A[2 * i]	[2 * j];
										if( sum == 2 || sum == -2 )
											tp1p++;
										else
											tp2p++;
									}
								}

								for( int i = 0; i < N; i++ )
								{
									temp = A[p2row1][i];
									A[p2row1][i] = A[p2row2][i];
									A[p2row2][i] = temp;
								}
								for( int i = 0; i < N; i++ )
								{
									temp = A[i][p2col1];
									A[i][p2col1] = A[i][p2col2];
									A[i][p2col2] = temp;
								}

								if( tp2p == maxtp2p && rnd.nextInt(2) == 0 )
								{
									maxtp2p = tp2p;
									maxg = g;
									maxh = h;
								}

								if( tp2p > maxtp2p )
								{
									maxtp2p = tp2p;
									maxg = g;
									maxh = h;
								}

							}
						}

						if( maxg == 1 )
						{
							p2row1 = p2rowblock1 * 2 + 1;
							p2col1 = p2colblock1 * 2 + 1;
						}
						if( maxg == 2 )
						{
							p2row1 = p2rowblock1 * 2 + 1;
							p2col1 = p2colblock1 * 2;
						}
						if( maxg == 3 )
						{
							p2row1 = p2rowblock1 * 2;
							p2col1 = p2colblock1 * 2 + 1;
						}
						if( maxg == 4 )
						{
							p2row1 = p2rowblock1 * 2;
							p2col1 = p2colblock1 * 2;
						}

						if( maxh == 1 )
						{
							p2row2 = p2rowblock2 * 2 + 1;
							p2col2 = p2colblock2 * 2 + 1;
						}
						if( maxh == 2 )
						{
							p2row2 = p2rowblock2 * 2 + 1;
							p2col2 = p2colblock2 * 2;
						}
						if( maxh == 3 )
						{
							p2row2 = p2rowblock2 * 2;
							p2col2 = p2colblock2 * 2 + 1;
						}
						if( maxh == 4 )
						{
							p2row2 = p2rowblock2 * 2;
							p2col2 = p2colblock2 * 2;
						}

					}

					p2ready = 1;
				}
				if( p2ready == 1 && p2m == 0 && p2go == 0 )
				{
					p2destr = p2row1;
					p2destc = p2col1;
					p2go = 1;
				}
				if( p2ready == 1 && p2m == 1 && p2go == 0 )
				{
					p2destr = p2row2;
					p2destc = p2col2;
					p2ready = 0;
					p2go = 1;
				}
			}

			if( p2strategy == 6 ){
				int i, j, k, y, z;
				int temp, temp1, temp2;

				// the main loop, it's finished if all the blocks are green
				// or if every combination has been tested

				// this was a while loop
				if( level >= 0 && level < N/2 && !done )
				{
					// first decide which column to put in column 1
					if( level == 0 )
					{
						if( !firstiteration )
							colused[firstchoice] = 0;
						firstiteration = false;
						if( firstchoice < 0 )
							firstchoice = 0;
						firstchoice++;
						while( colused[firstchoice] == 1 ) // this should be unnecessary
							firstchoice++;
						//colindex[1] = firstchoice;
						swap( 0, 1, 0, colcurrpos[firstchoice] );
						colused[firstchoice] = 1;
					}
					// next decide which column to pair with the next available column
					else if( level > 0 && level <= numlevels )
					{
						// first check if the left column needs to be updated
						//  if it does, then put the next available column there
						if( nextcol[level] == -1 )
						{
							firstiteration = true;
							nextcol[level] = 1;
							while( colused[nextcol[level]] == 1 )
								nextcol[level]++;
							//colindex[level*2] = nextcol[level];
							swap( 0, level*2, 0, colcurrpos[nextcol[level]] );
							leftcol[level] = nextcol[level];
							colused[nextcol[level]] = 1;
						}
						// now put the column whose turn is next in the right column.
						// we can assume that there is a next column available, since
						//  there is code later that checks if there are none left
						// if this is the first iteration, then don't remove the previous column
						if( !firstiteration )
							colused[nextcol[level]] = 0;
						firstiteration = false;
						nextcol[level]++;
						while( colused[nextcol[level]] == 1 )
							nextcol[level]++;
						//colindex[level*2+1] = nextcol[level];
						swap( 0, level*2+1, 0, colcurrpos[nextcol[level]] );
						colused[nextcol[level]] = 1;
					}

					// now test if this combination is still acceptable
					goback = false;

					// initialize the arrays
					for( i = 0; i < N; i++ )
					{
						binaryval[i] = 0;
						otherbinaryval[i] = 0;
						tag[i] = i;
					}

					// store the binary interpretation of each row
					for( i = 0; i < N; i++ )
					{
						// examine each pair of values in ths row
						// treat each match as a 0 and each nonmatch as a 1
						for( j = 0; j <= level; j++ )
						{
							if( Math.abs(	A[i][j*2  ]
									+ A[i][j*2+1] ) != 0 )
							{
								binaryval[i]++;
							}
							else
							{
								otherbinaryval[i]++;
							}
							if( j < level )
							{
								binaryval[i] *= 2;
								otherbinaryval[i] *= 2;
							}
						}
						//			System.out.println( "" + binaryval[i] + " " + otherbinaryval[i] );
						//			System.out.println();
					}

					// now sort the rows according to their binary value
					// the first half will be sorted by binaryvalue and put in the even columns
					// the second half will be sorted by their otherbinaryvalue and put in the odd columns
					// using a bubble sort for now
					i = 0;
					while( i < N-1 )
					{
						while( binaryval[i] > binaryval[i+1] )
						{
							temp = binaryval[i];
							binaryval[i] = binaryval[i+1];
							binaryval[i+1] = temp;
							temp = otherbinaryval[i];
							otherbinaryval[i] = otherbinaryval[i+1];
							otherbinaryval[i+1] = temp;
							temp = tag[i];
							tag[i] = tag[i+1];
							tag[i+1] = temp;

							if( i > 0 )
								i--;
						}
						i++;
					}

					i = N/2;
					while( i < N-1 )
					{
						while( otherbinaryval[i] > otherbinaryval[i+1] )
						{
							temp = binaryval[i];
							binaryval[i] = binaryval[i+1];
							binaryval[i+1] = temp;
							temp = otherbinaryval[i];
							otherbinaryval[i] = otherbinaryval[i+1];
							otherbinaryval[i+1] = temp;
							temp = tag[i];
							tag[i] = tag[i+1];
							tag[i+1] = temp;

							if( i > N/2 )
								i--;
						}
						i++;
					}

					// now check if each pair of rows matches
					// a pair of rows matches if each row's binaryval is the other row's otherbinaryval
					for( i = 0; i < N/2; i++ )
					{
						//			System.out.print( colused[i] + "	" );
						//			System.out.print( ""  + binaryval[		i] + " " + otherbinaryval[		i] );
						//			System.out.print( " " + binaryval[(N/2)+i] + " " + otherbinaryval[(N/2)+i] );
						if( binaryval[i] != otherbinaryval[N/2+i] )
						{
							goback = true;
							//			  System.out.print( " *" );
						}
						//			System.out.println();
					}

					if( standardize )
					{
						// put the rows in the order of their tags
						// even though this invaliates the information in rowindex[]

						int r1 = 0;
						int c1 = 0;
						int r2 = 0;
						int c2 = 0;

						int[] rightrow = new int[N];
						for( int h = 0; h < N; h++ )
						{
							rowindex[h] = h;
							rowcurrpos[h] = h;
						}
						for( int h = 0; h < N/2; h++ )
						{
							rightrow[2*h  ] = tag[h];
							rightrow[2*h+1] = tag[h+(N/2)];
						}

						// now put the rows and columns in the correct order
						for( int h = 0; h < N; h++ )
						{
							//swap( r, c, rowindex[r], colindex[c] );
							r1 = h;
							c1 = 0;
							r2 = rowcurrpos[rightrow[h]];
							c2 = 0;
							temp = 0;

							for( int i2 = 0; i2 < N; i2++ )
							{
								temp = A[r1][i2];
								A[r1][i2] = A[r2][i2];
								A[r2][i2] = temp;
							}
							checkscore();

							rowcurrpos[rowindex[r1]] = r2;
							rowcurrpos[rowindex[r2]] = r1;

							temp = rowindex[r1];
							rowindex[r1] = rowindex[r2];
							rowindex[r2] = temp;
						}
					}

					// move to the next level only if this level is all green
					if( !goback )
					{
						level++;
					}
					else
					{
						finishedpopping = false;
						while( !finishedpopping )
						{
							// check if the next iteration is valid
							nextfirstchoice = firstchoice;
							if( nextfirstchoice >= 0 )
								while( nextfirstchoice < N && colused[nextfirstchoice] != 0 )
									nextfirstchoice++;
							nextnextcol = nextcol[level];
							if( nextnextcol >= 0 )
								while( nextnextcol < N && colused[nextnextcol] != 0 )
									nextnextcol++;
							// the matrix is unsolveable if the first level has been exhausted
							if( level == 0 && nextfirstchoice >= N )
							{
								done = true;
								success = false;
								finishedpopping = true;
							}
							// check if the matrix has been solved
							//			  else if( p2p == N/2 * N/2 )
							//			  {
							//				done = true;
							//				success = true;
							//				finishedpopping = true;
							//			  }
							// move down a level if the current level is exhausted
							else if( level > 0 && level <= numlevels &&	 nextnextcol >= N )
							{
								// free the active two columns
								colused[leftcol[level]] = 0;
								colused[nextcol[level]] = 0;
								leftcol[level] = -1;
								nextcol[level] = -1;
								level--;
							}
							else
								finishedpopping = true;
						}
					}
					//		  System.out.println( level );
					//		  if( level == 10 )
					//			usingTimer = false;
				}
				else
				{
					if( level >= N/2 )
						success = true;

					if( success )
					{
						//if( !answeronly )
						System.out.println( "Success!\n\n" );
						p2a = 0;
						p2strategy = 0;
					}
					else
						System.out.println( "Failure!\n\n" );
				}

				refresh = 1;
			}

			if( p2strategy == 7 )
			{
				int i, j, k, y, z;
				int temp, temp1, temp2;

				// the main loop, it's finished if all the blocks are green
				// or if every combination has been tested
				// this was a while loop
				while( level >= 0 && level < N/2 && !done )
				{
					// first decide which column to put in column 1
					if( level == 0 )
					{
						if( !firstiteration )
							colused[firstchoice] = 0;
						firstiteration = false;
						if( firstchoice < 0 )
							firstchoice = 0;
						firstchoice++;
						while( colused[firstchoice] == 1 ) // this should be unnecessary
							firstchoice++;
						colindex[1] = firstchoice;
						//swap( 0, 1, 0, colcurrpos[firstchoice] );
						colused[firstchoice] = 1;
					}
					// next decide which column to pair with the next available column
					else if( level > 0 && level <= numlevels )
					{
						// first check if the left column needs to be updated
						//  if it does, then put the next available column there
						if( nextcol[level] == -1 )
						{
							firstiteration = true;
							nextcol[level] = 1;
							while( colused[nextcol[level]] == 1 )
								nextcol[level]++;
							colindex[level*2] = nextcol[level];
							//swap( 0, level*2, 0, colcurrpos[nextcol[level]] );
							leftcol[level] = nextcol[level];
							colused[nextcol[level]] = 1;
						}
						// now put the column whose turn is next in the right column.
						// we can assume that there is a next column available, since
						//  there is code later that checks if there are none left
						// if this is the first iteration, then don't remove the previous column
						if( !firstiteration )
							colused[nextcol[level]] = 0;
						firstiteration = false;
						nextcol[level]++;
						while( colused[nextcol[level]] == 1 )
							nextcol[level]++;
						colindex[level*2+1] = nextcol[level];
						//swap( 0, level*2+1, 0, colcurrpos[nextcol[level]] );
						colused[nextcol[level]] = 1;
					}

					// now test if this combination is still acceptable
					goback = false;

					// initialize the arrays
					for( i = 0; i < N; i++ )
					{
						binaryval[i] = 0;
						otherbinaryval[i] = 0;
						tag[i] = i;
					}

					// store the binary interpretation of each row
					for( i = 0; i < N; i++ )
					{
						// examine each pair of values in ths row
						// treat each match as a 0 and each nonmatch as a 1
						for( j = 0; j <= level; j++ )
						{
							if( Math.abs(	A[i][colindex[j*2  ]]
									+ A[i][colindex[j*2+1]] ) != 0 )
							{
								binaryval[i]++;
							}
							else
							{
								otherbinaryval[i]++;
							}
							if( j < level )
							{
								binaryval[i] *= 2;
								otherbinaryval[i] *= 2;
							}
						}
						//			System.out.println( "" + binaryval[i] + " " + otherbinaryval[i] );
						//			System.out.println();
					}

					// now sort the rows according to their binary value
					// the first half will be sorted by binaryvalue and put in the even columns
					// the second half will be sorted by their otherbinaryvalue and put in the odd columns
					// using a bubble sort for now
					i = 0;
					while( i < N-1 )
					{
						while( binaryval[i] > binaryval[i+1] )
						{
							temp = binaryval[i];
							binaryval[i] = binaryval[i+1];
							binaryval[i+1] = temp;
							temp = otherbinaryval[i];
							otherbinaryval[i] = otherbinaryval[i+1];
							otherbinaryval[i+1] = temp;
							temp = tag[i];
							tag[i] = tag[i+1];
							tag[i+1] = temp;

							if( i > 0 )
								i--;
						}
						i++;
					}

					i = N/2;
					while( i < N-1 )
					{
						while( otherbinaryval[i] > otherbinaryval[i+1] )
						{
							temp = binaryval[i];
							binaryval[i] = binaryval[i+1];
							binaryval[i+1] = temp;
							temp = otherbinaryval[i];
							otherbinaryval[i] = otherbinaryval[i+1];
							otherbinaryval[i+1] = temp;
							temp = tag[i];
							tag[i] = tag[i+1];
							tag[i+1] = temp;

							if( i > N/2 )
								i--;
						}
						i++;
					}

					// now check if each pair of rows matches
					// a pair of rows matches if each row's binaryval is the other row's otherbinaryval
					for( i = 0; i < N/2; i++ )
					{
						//			System.out.print( colused[i] + "	" );
						//			System.out.print( ""  + binaryval[		i] + " " + otherbinaryval[		i] );
						//			System.out.print( " " + binaryval[(N/2)+i] + " " + otherbinaryval[(N/2)+i] );
						if( binaryval[i] != otherbinaryval[N/2+i] )
						{
							goback = true;
							//			  System.out.print( " *" );
						}
						//			System.out.println();
					}

					if( standardize )
					{
						// put the rows in the order of their tags
						// even though this invaliates the information in rowindex[]

						int r1 = 0;
						int c1 = 0;
						int r2 = 0;
						int c2 = 0;

						int[] rightrow = new int[N];
						for( int h = 0; h < N; h++ )
						{
							rowindex[h] = h;
							rowcurrpos[h] = h;
						}
						for( int h = 0; h < N/2; h++ )
						{
							rightrow[2*h  ] = tag[h];
							rightrow[2*h+1] = tag[h+(N/2)];
						}

						// now put the rows and columns in the correct order
						for( int h = 0; h < N; h++ )
						{
							//swap( r, c, rowindex[r], colindex[c] );
							r1 = h;
							c1 = 0;
							r2 = rowcurrpos[rightrow[h]];
							c2 = 0;
							temp = 0;

							for( int i2 = 0; i2 < N; i2++ )
							{
								temp = A[r1][i2];
								A[r1][i2] = A[r2][i2];
								A[r2][i2] = temp;
							}
							checkscore();

							rowcurrpos[rowindex[r1]] = r2;
							rowcurrpos[rowindex[r2]] = r1;

							temp = rowindex[r1];
							rowindex[r1] = rowindex[r2];
							rowindex[r2] = temp;
						}
					}

					// move to the next level only if this level is all green
					if( !goback )
					{
						level++;
					}
					//		  else if( p2p == N/2 * N/2 )
					//		  {
					//			done = true;
					//			success = true;
					//		  }
					else
					{
						finishedpopping = false;
						while( !finishedpopping )
						{
							// check if the next iteration is valid
							nextfirstchoice = firstchoice;
							if( nextfirstchoice >= 0 )
								while( nextfirstchoice < N && colused[nextfirstchoice] != 0 )
									nextfirstchoice++;
							nextnextcol = nextcol[level];
							if( nextnextcol >= 0 )
								while( nextnextcol < N && colused[nextnextcol] != 0 )
									nextnextcol++;
							// the matrix is unsolveable if the first level has been exhausted
							if( level == 0 && nextfirstchoice >= N )
							{
								done = true;
								success = false;
								finishedpopping = true;
							}
							// check if the matrix has been solved
							//			  else if( p2p == N/2 * N/2 )
							//			  {
							//				done = true;
							//				success = true;
							//				finishedpopping = true;
							//			  }
							// move down a level if the current level is exhausted
							else if( level > 0 && level <= numlevels &&	 nextnextcol >= N )
							{
								// free the active two columns
								colused[leftcol[level]] = 0;
								colused[nextcol[level]] = 0;
								leftcol[level] = -1;
								nextcol[level] = -1;
								level--;
							}
							else
								finishedpopping = true;
						}
					}
					//		  System.out.println( level );
					//		  if( level == 10 )
					//			usingTimer = false;
				}

				{
					if( level >= N/2 )
						success = true;

					if( success ){
						int r1 = 0;
						int c1 = 0;
						int r2 = 0;
						int c2 = 0;

						int[] rightcol = new int[N];
						for( int h = 0; h < N; h++ )
						{
							rightcol[h] = colindex[h];
							colindex[h] = h;
						}
						//int temp;
						//if( !answeronly )
						System.out.println( "Success!\n\n" );

						// now put the rows and columns in the correct order
						for( int h = 0; h < N; h++ ){
							//swap( r, c, rowindex[r], colindex[c] );
							r1 = 0;
							c1 = h;
							r2 = 0;
							c2 = colcurrpos[rightcol[h]];
							temp = 0;

							for( int i2 = 0; i2 < N; i2++ )
							{
								temp = A[i2][c1];
								A[i2][c1] = A[i2][c2];
								A[i2][c2] = temp;
							}
							checkscore();

							colcurrpos[colindex[c1]] = c2;
							colcurrpos[colindex[c2]] = c1;

							temp = colindex[c1];
							colindex[c1] = colindex[c2];
							colindex[c2] = temp;
						}
					}
					else
						System.out.println( "Failure!\n\n" );
				}

				p2a = 0;
				p2strategy = 0;
				refresh = 1;
			}

			// this is the code that "presses the buttons" if player 2 is automated
			if( p2strategy >= 2 && p2a == 1 )
			{
				if( p2go == 1 && p2movecheat == 0 )
				{
					int p2r = 0;
					int p2c = 0;
					if( p2m == 0 )
					{
						p2r = p2r1;
						p2c = p2c1;
					}
					else
					{
						p2r = p2r2;
						p2c = p2c2;
					}
					if( p2r < p2destr && rnd.nextInt(p2move) == 0 )
						k5 = true;
					if( p2r > p2destr && rnd.nextInt(p2move) == 0 )
						k8 = true;
					if( p2c < p2destc && rnd.nextInt(p2move) == 0 )
						k6 = true;
					if( p2c > p2destc && rnd.nextInt(p2move) == 0 )
						k4 = true;
					if( p2r == p2destr && p2c == p2destc && rnd.nextInt(p2choose) == 0 )
					{
						k0 = true;
						p2go = 0;
					}
				}
				if( p2go == 1 && p2movecheat == 1 )
				{
					if( p2m == 0 )
					{
						p2r1 = p2destr;
						p2c1 = p2destc;
					}
					else
					{
						p2r2 = p2destr;
						p2c2 = p2destc;
					}
					k0 = true;
					p2go = 0;
				}
			}

			if( p2m == 0 )
			{
				if( k4 )
				{
					k4 = false;
					p2c1--;
					refresh = 1;
					p2cyclenum = 0;
				}
				if( k6 )
				{
					k6 = false;
					p2c1++;
					refresh = 1;
					p2cyclenum = 0;
				}
				if( k8 )
				{
					k8 = false;
					p2r1--;
					refresh = 1;
					p2cyclenum = 0;
				}
				if( k5 )
				{
					k5 = false;
					p2r1++;
					refresh = 1;
					p2cyclenum = 0;
				}
			}
			else
			{
				if( k4 )
				{
					k4 = false;
					p2c2--;
					refresh = 1;
					p2cyclenum = 0;
				}
				if( k6 )
				{
					k6 = false;
					p2c2++;
					refresh = 1;
					p2cyclenum = 0;
				}
				if( k8 )
				{
					k8 = false;
					p2r2--;
					refresh = 1;
					p2cyclenum = 0;
				}
				if( k5 )
				{
					k5 = false;
					p2r2++;
					refresh = 1;
					p2cyclenum = 0;
				}
			}

			// switch the current block to a different state
			// this cycles through all 4 possible states
			/*
	if( A$ = "G" || B$ = "G" )

	  p2cyclenum = p2cyclenum + 1
	  if( p2cyclenum > 3 ) p2cyclenum = 1

	  if( p2cyclenum = 1 )
		if( (p2r1 % 2) = 0 )
		  p2r1 = p2r1 - 1
		else
		  p2r1 = p2r1 + 1
		}
		if( (p2c1 % 2) = 0 )
		  p2c1 = p2c1 - 1
		else
		  p2c1 = p2c1 + 1
		}
	  }
	  if( p2cyclenum = 2 )
		if( (p2r1 % 2) = 0 )
		  p2r1 = p2r1 - 1
		else
		  p2r1 = p2r1 + 1
		}
		if( (p2c1 % 2) = 0 )
		  p2c1 = p2c1 - 1
		else
		  p2c1 = p2c1 + 1
		}
		if( (p2r2 % 2) = 0 )
		  p2r2 = p2r2 - 1
		else
		  p2r2 = p2r2 + 1
		}
		if( (p2c2 % 2) = 0 )
		  p2c2 = p2c2 - 1
		else
		  p2c2 = p2c2 + 1
		}
	  }
	  if( p2cyclenum = 3 )
		if( (p2r1 % 2) = 0 )
		  p2r1 = p2r1 - 1
		else
		  p2r1 = p2r1 + 1
		}
		if( (p2c1 % 2) = 0 )
		  p2c1 = p2c1 - 1
		else
		  p2c1 = p2c1 + 1
		}
	  }

	  p2m = 1
	  A$ = "F"
	}
			 */
		} // end if
		keyPressed = false;

		int temp;

		if( kf )
		{
			kf = false;
			refresh = 1;
			if( p1m == 0 )
			{
				p1m = 1;
				p1r2 = p1r1;
				p1c2 = p1c1;
			}
			else
			{
				swap( p1r1, p1c1, p1r2, p1c2 );

				p1m = 0;

				temp = p1r1;
				p1r1 = p1r2;
				p1r2 = temp;
				temp = p1c1;
				p1c1 = p1c2;
				p1c2 = temp;
			}
		}
		if( kd )
		{
			kd = false;
			refresh = 1;
			p1cyclenum = 0;
			p1m = 0;
			p1r1 = p1r2;
			p1c1 = p1c2;
		}

		if( p1r1 < 0 )
			p1r1 = N-1;
		if( p1r1 >= N )
			p1r1 = 0;
		if( p1c1 < 0 )
			p1c1 = N-1;
		if( p1c1 >= N )
			p1c1 = 0;
		if( p1r2 < 0 )
			p1r2 = N-1;
		if( p1r2 >= N )
			p1r2 = 0;
		if( p1c2 < 0 )
			p1c2 = N-1;
		if( p1c2 >= N )
			p1c2 = 0;

		if( k0 )
		{
			k0 = false;
			refresh = 1;
			if( p2m == 0 )
			{
				p2m = 1;
				p2r2 = p2r1;
				p2c2 = p2c1;
			}
			else
			{
				swap( p2r1, p2c1, p2r2, p2c2 );

				p2m = 0;

				temp = p2r1;
				p2r1 = p2r2;
				p2r2 = temp;
				temp = p2c1;
				p2c1 = p2c2;
				p2c2 = temp;
			}
		}
		if( kp )
		{
			kp = false;
			refresh = 1;
			p2cyclenum = 0;
			p2m = 0;
			p2r1 = p2r2;
			p2c1 = p2c2;
		}

		if( p2r1 < 0 )
			p2r1 = N-1;
		if( p2r1 >= N )
			p2r1 = 0;
		if( p2c1 < 0 )
			p2c1 = N-1;
		if( p2c1 >= N )
			p2c1 = 0;
		if( p2r2 < 0 )
			p2r2 = N-1;
		if( p2r2 >= N )
			p2r2 = 0;
		if( p2c2 < 0 )
			p2c2 = N-1;
		if( p2c2 >= N )
			p2c2 = 0;

		// this code stops the computer players if they have won
		/*
	if( p1p == N / 2 * N / 2 || p2p == N / 2 * N / 2 )
	{
	  p1a = 0;
	  p2a = 0;
	}
		 */

		if( p1p == N / 2 * N / 2 )
		{
			p1a = 0;
		}

		if( p2p == N / 2 * N / 2 )
		{
			p2a = 0;
		}

		/*
	// used for debugging
	if( time % 100 = 0 )
	{
	  HADsum = 0
	  for( int i = 0; i < N; i++ )
	  {
		for( int j = 0; j < N; j++ )
		{
		  if( A[i, j) != 1 && A[i, j) != -1 )
			HADsum = 9999
		  if( i != j )
		  {
			for( K = 1 TO N
			{
			  HADsum = HADsum + A[i, K) * A[j, K)
			}
			if( HADsum != 0 )
			{
			  for( L = 1 TO 1000000
			  NEXT L
			}
		  }
		}
	  }
	}
		 */

		if( refresh == 1 )
		{
			updateImage();
			repaint();
		}
		refresh = 0;

	}

	public void swap( int r1, int c1, int r2, int c2 )
	{
		int temp = 0;

		for( int i = 0; i < N; i++ )
		{
			temp = A[r1][i];
			A[r1][i] = A[r2][i];
			A[r2][i] = temp;
		}
		for( int i = 0; i < N; i++ )
		{
			temp = A[i][c1];
			A[i][c1] = A[i][c2];
			A[i][c2] = temp;
		}
		checkscore();

		rowcurrpos[rowindex[r1]] = r2;
		rowcurrpos[rowindex[r2]] = r1;
		colcurrpos[colindex[c1]] = c2;
		colcurrpos[colindex[c2]] = c1;

		temp = rowindex[r1];
		rowindex[r1] = rowindex[r2];
		rowindex[r2] = temp;
		temp = colindex[c1];
		colindex[c1] = colindex[c2];
		colindex[c2] = temp;
	}


	public int readInt(BufferedReader reader)
	{
		String inputString = readLine(reader);
		int n = Integer.parseInt(inputString);
		return n;
	}
	public double readDouble(BufferedReader reader)
	{
		String inputString = readLine(reader);
		double x = Double.parseDouble(inputString);
		return x;
	}
	public String readLine(BufferedReader reader)
	{
		String inputLine = "";
		try
		{
			inputLine = reader.readLine();
		}
		catch(IOException e)
		{
			System.out.println(e);
			dispose();
		}
		return inputLine;
	}
	public boolean EOF(BufferedReader reader)
	{
		String inputLine = "";
		boolean eof = false;
		try
		{
			reader.mark(1);
			inputLine = reader.readLine();
			eof = inputLine == null;
			reader.reset();
		}
		catch(IOException e)
		{
			System.out.println(e);
			//		dispose();
		}
		return eof;
	}

	private int width; // the width of the drawing area
	private int height; // the height of the drawing area

	Timer timer;

	private Random rnd;

	private int  N = 24; // the size of the matrix
	private int [][] A; // the main matrix
	private int [][] B; // stores the rank of each block in matrix A
	private String filename = "had.24.1.txt";
	private int board = 1; // currently unused

	private int p1r1 = 0; // player 1's first selected row
	private int p1c1 = 0; // player 1's first selected column
	private int p1r2 = 0; // player 1's second selected row
	private int p1c2 = 0; // player 1's second selected column
	private int p1p = 0; // player 1's points
	private int p1m = 0; // 1 iff player 1's second marker is active

	// same as for player 1
	private int p2r1 = N-1;
	private int p2c1 = N-1;
	private int p2r2 = N-1;
	private int p2c2 = N-1;
	private int p2p = 0;
	private int p2m = 0;

	private int refresh = 1; // 1 iff the screen needs to be repainted
	private int size = 400 / N; // the size of the drawing area

	private int p1a = 0; // 1 iff player is automated
	private int p1movecheat = 1; // 1 iff the automated player isn't restricted to moving only by pressing keys
	private int p1move = 1000 / 1000; // twice the average delay between the automated player moves
	private int p1choose = 1000 / 1000; // twice the average delay before the automated player chooses a square
	private int p1think = 1000 / 1000; // twice the average delay before the automated player chooses a destination
	private int p1strategy = 4; // the automated player's strategy

	private int p1cyclenum = 0; // used to cycle a block through all 4 possible states
	private int p1destr = 1; // the automated player's destination row
	private int p1destc = 1; // the automated player's destination column
	private int p1ready = 0; // 1 iff the automated player is ready to set its destination
	private int p1go = 0; // 1 iff the automated player's destination is set

	private int p1row1 = 0; // the first row that the automated player will choose
	private int p1rowblock1 = 0; // the first row of blocks that the automated player will choose
	private int row1score = 0; // the score of the first row of blocks being considered
	private int p1col1 = 0; // the first column that the automated player will choose
	private int p1colblock1 = 0; // the first column of blocks that the automated player will choose
	private int col1score = 0; // the score of the first column of blocks being considered
	private int p1row2 = 0; // the second row that the automated player will choose
	private int p1rowblock2 = 0; // the second row of blocks that the automated player will choose
	private int row2score = 0; // the score of the second row of blocks being considered
	private int p1col2 = 0; // the second column that the automated player will choose
	private int p1colblock2 = 0; // the second column of blocks that the automated player will choose
	private int col2score = 0; // the score of the second column of blocks being considered

	// same as for player 1
	private int p2a = 0;
	private int p2movecheat = 1;
	private int p2move = 1000 / 1000;
	private int p2choose = 1000 / 1000;
	private int p2think = 1000 / 1000;
	private int p2strategy = 2;

	private int p2cyclenum = 0;
	private int p2destr = 1;
	private int p2destc = 1;
	private int p2ready = 0;
	private int p2go = 0;

	private int p2row1 = 0; // the first row that the automated player will choose
	private int p2rowblock1 = 0; // the first row of blocks that the automated player will choose
	//private int row1score = 0; // the score of the first row of blocks being considered
	private int p2col1 = 0; // the first column that the automated player will choose
	private int p2colblock1 = 0; // the first column of blocks that the automated player will choose
	//private int col1score = 0; // the score of the first column of blocks being considered
	private int p2row2 = 0; // the second row that the automated player will choose
	private int p2rowblock2 = 0; // the second row of blocks that the automated player will choose
	//private int row2score = 0; // the score of the second row of blocks being considered
	private int p2col2 = 0; // the second column that the automated player will choose
	private int p2colblock2 = 0; // the second column of blocks that the automated player will choose
	//private int col2score = 0; // the score of the second column of blocks being considered

	private int time = 0; // the number of turns since the game was started

	private boolean ki;
	private boolean kk;
	private boolean kj;
	private boolean kl;
	private boolean kf;
	private boolean kd;

	private boolean k8;
	private boolean k5;
	private boolean k4;
	private boolean k6;
	private boolean kp;
	private boolean k0;

	boolean titlescreen;

	boolean usingTimer;
	boolean keyPressed;

	int xOffset = 5;
	int yOffset = 25;

	private BufferedReader reader;
	JOptionPane JOptionPane1 = new JOptionPane();

	BufferedImage image = new BufferedImage( 100, 100, BufferedImage.TYPE_INT_ARGB );

	Block2by2 parent;

	// the following variables are used only by the exhaustive search algorithm

	boolean sortingrows = false; // true iff the rows are being sorted
	boolean rowsortloop2 = false; // true iff the second row-sorting loop is active
	boolean previousswap = false; // true iff the current step requires a second swap
	int savedi = 0; // the variable i needed to be saved
	int[] rowcurrpos; // the current position of the row originally in each position
	int[] colcurrpos; // the current position of the column originally in each position

	int numposcols = 0; // the number of positive columns
	int numnegcols = 0; // the number of negative columns
	int numposlevels = 0; // the number of pairs of positive columns above level 0
	int numneglevels = 0; // the number of pairs of negative columns
	int[] poscolindex; // the current index of each of the positive columns
	int[] negcolindex; // the current index of each of the negative columns
	int[] poscolused; // stores which positive columns are used
	int[] negcolused; // stores which negative columns are used

	int level = 0; // how far into the matrix the entry being tested is
	int[] rowindex; // the current index of each row
	int[] colindex; // the current index of each column
	int firstchoice = -1; // which positive column is put in column 1
	int nextfirstchoice = -1; // used to check if there is a valid next first choice
	int[] nextposcol; // the number of the positive columns currently placed in the odd columns
	int[] nextnegcol; // the number of the negative columns currently placed in the odd columns
	int nextnextposcol = -1; // used to check if there is a valid next positive column
	int nextnextnegcol = -1; // used to check if there is a valid next negative column
	int[] leftposcol; // the positive column placed in the left column of each level
	int[] leftnegcol; // the negative column placed in the left column of each level
	int[][] sizes; // the size of each group at each level
	int groupnum = 0; // the number of the next group to process
	int ourgroupnum = 0; // the number of the group we are adding to the current level
	int groupsize = 0; // the size of the group we are working with
	boolean done = false; // true if every possible combination has been tested
	boolean success = false; // true if the matrix has been converted to all blue

	int numdiffs = 0; // the number of rows in the group whose entries are different
	int numdiffs2 = 0; // used in standardizing
	int groupstart = 0; // the index of the first row in the group
	int groupend = N-1; // the index of the last row in the group
	boolean goback = false; // true if it is impossible to proceed to the next level
	boolean firstiteration = true; // a flag that is set on the first iteration at each level, then immediately cleared
	boolean finishedpopping = true; // true if there is no need to back any more levels

	boolean standardize = true;
	
	int selectedPlayer = 1;

	// the following variables are used only by the green exhaustive search algorithm
	
	int numlevels; // the total number of pairs of columns past level 0
	int[] colused; // stores which columns are used

	int[] nextcol; // the number of the columns currently placed in the odd columns
	int nextnextcol = -1; // used to check if there is a valid next positive column
	int[] leftcol; // the positive column placed in the left column of each level

	int[] binaryval; // used for sorting the rows
	int[] otherbinaryval; // used for sorting the rows
	int[] tag; // used for sorting the rows, stores the original position of each row
}